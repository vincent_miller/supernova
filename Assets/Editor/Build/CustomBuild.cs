using UnityEngine;
using UnityEditor;
using System.IO;
using System.IO.Compression;
using System.Collections;
using Ionic.Zip;

class CustomBuild : EditorWindow {

	bool win32 = true;
	bool win64 = true;
	bool osx = true;
	bool linux = true;
	bool web = true;
	string version = "0.0.0";
	string buildName = "Supernova";
	string[] levels = { "Assets/scenes/MainMenu.unity", "Assets/scenes/Deathmatch.unity", "Assets/scenes/Escort.unity" };

	
	[MenuItem ("File/Custom Build")]
	public static void  ShowWindow () {
		EditorWindow.GetWindow(typeof(CustomBuild));
	}
	
	void OnGUI () {
		GUILayout.Label ("Build Targets", EditorStyles.boldLabel);
		win32 = EditorGUILayout.Toggle ("Windows 32-bit", win32);
		win64 = EditorGUILayout.Toggle ("Windows 64-bit", win64);
		osx = EditorGUILayout.Toggle ("Mac OSX", osx);
		linux = EditorGUILayout.Toggle ("Linux", linux);
		web = EditorGUILayout.Toggle ("Web Player", web);

		GUILayout.Label ("Build Version (MAJOR.MINOR.PATCH)", EditorStyles.boldLabel);
		version = EditorGUILayout.TextField (version);
		GUILayout.Label ("Name", EditorStyles.boldLabel);
		buildName = EditorGUILayout.TextField (buildName);

		if (GUILayout.Button ("Build")) {
			string path = EditorUtility.SaveFolderPanel ("Build Output Folder", "", "");
			if (path.Length > 0) {
				ExecuteBuild (path);
			}
		}
	}

	void ExecuteBuild (string folder) {
		if (win32) {
			EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.StandaloneWindows);
			BuildPipeline.BuildPlayer(levels, Path.Combine (folder, "win32") + "/" + buildName + ".exe", BuildTarget.StandaloneWindows, BuildOptions.None);
		}

		if (win64) {
			EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.StandaloneWindows64);
			BuildPipeline.BuildPlayer (levels, Path.Combine (folder, "win64") + "/" + buildName + ".exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
		}

		if (osx) {
			EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.StandaloneOSXUniversal);
			BuildPipeline.BuildPlayer (levels, Path.Combine (folder, "osx") + "/" + buildName, BuildTarget.StandaloneOSXUniversal, BuildOptions.None);
		}

		if (linux) {
			EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.StandaloneLinuxUniversal);
			BuildPipeline.BuildPlayer (levels, Path.Combine (folder, "linux") + "/" + buildName, BuildTarget.StandaloneLinuxUniversal, BuildOptions.None);
		}

		if (web) {
			EditorUserBuildSettings.SwitchActiveBuildTarget (BuildTarget.WebPlayer);
			BuildPipeline.BuildPlayer (levels, Path.Combine (folder, "web") + "/" + buildName, BuildTarget.WebPlayer, BuildOptions.None);
		}

		if (win32) {
			FinishDirectory (folder, buildName, "win32", version);
		}
		
		if (win64) {
			FinishDirectory (folder, buildName, "win64", version);
		}
			
		if (osx) {
			FinishDirectory (folder, buildName, "osx", version);
		}
				
		if (linux) {
			FinishDirectory (folder, buildName, "linux", version);
		}
	}

	void FinishDirectory (string folder, string buildName, string platform, string version) {
		File.Copy ("Assets/README.txt", Path.Combine (folder, platform) + "/README.txt");
		ZipFile zip = new ZipFile (Path.Combine(folder, buildName + "_" + platform + "_" + version + ".zip"));
		zip.ParallelDeflateThreshold = -1;
		zip.AddDirectory (Path.Combine(folder, platform));
		zip.Save();
	}
}