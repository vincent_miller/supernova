using UnityEngine;
using System.Collections.Generic;

public class AICruiserControl : MonoBehaviour {
	private Queue<CruiserDirective> directives = new Queue<CruiserDirective>();

	private Cruiser Cruiser {
		get {
			return GetComponent<Cruiser> ();
		}
	}

	// Use this for initialization
	void Start () {
	
	}

	public void AddDirective(CruiserDirective directive) {
		directives.Enqueue(directive);
		directive.Start(Cruiser);
	}

	public bool HasDirectives() {
		return directives.Count > 0;
	}

	public void ClearDirectives() {
		directives.Clear();
	}

	// Update is called once per frame
	void Update () {
		if (GlobalController.Paused || !Cruiser.Ready) {
			return;
		}

		if (directives.Count > 0 && Cruiser.Alive) {
			CruiserDirective d = directives.Peek();
			d.Update(Cruiser);
			if (d.Done(Cruiser)) {
				directives.Dequeue();
			}
		} else if (!(Cruiser.Human && Cruiser.PlayerControl.EnginesActive)) {
			Cruiser.CancelThrust();
		}


        if (!(Cruiser.Human && Cruiser.PlayerControl.EnginesActive && Cruiser.PlayerControl.DirectTurretMode)) {
            for (int i = 0; i < Cruiser.NumTurrets; i++) {
                Turret turret = Cruiser.TurretAt(i);
                if (!Cruiser.PlayerControl.TurretActive(turret)) {
                    turret.AIUpdate();
                }
            }
        }
	}
}

