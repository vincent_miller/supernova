﻿using UnityEngine;
using System.Collections;

public abstract class Damageable : MonoBehaviour {
	public float MaxHealth;
	public float Health { get; protected set; }
	public GameObject Explosion;
    public Vector3 ExplosionRadius = Vector3.zero;

	public string DisplayName;

	private CooldownTimer Explode;
	
	public bool Human { get; protected set; }
	public float TimeToDie = 1.0f;
	
	protected ExpirationTimer ShowLastHit;
	protected Vector3 LastHit;

	public bool Alive { get; private set; }

	public Damageable() {
		Alive = true;
	}

	// Use this for initialization
	public void Start () {
		Alive = true;
		Health = MaxHealth;
		ShowLastHit = new ExpirationTimer (0.25f);
		Explode = new CooldownTimer(0.2f);
	}

	public void Update() {
        if (Explosion == null) {
            Explosion = GlobalController.ExplMed;
        }

		if (!Alive && Explode.Use) {
			float f = Explosion.GetComponent<Detonator>().size / 6.0f;
            Vector3 r = Random.insideUnitSphere;
            Instantiate(Explosion).transform.position = transform.TransformPoint(r * f + new Vector3(r.x * ExplosionRadius.x, r.y * ExplosionRadius.y, r.z * ExplosionRadius.z));
		}
	}

	public virtual void ZeroHealth() {
		Alive = false;
		Invoke ("Destroy", TimeToDie);
	}

	public virtual void Destroy() {
		if (Human) {
			//GlobalController.ReleaseControl ();
		}
        
		Instantiate (Explosion).transform.position = transform.position;
		Alive = false;

		if (Human) {
			GlobalController.CurrentCamera.transform.parent = null;
		}
		Destroy (gameObject);
	}

	protected virtual void OnTakeDamage(float amount, Transform source) {

	}
	
	public void Damage(float damage, Transform source) {
		Health -= damage;
		LastHit = source.position;
		ShowLastHit.Set ();
		OnTakeDamage (damage, source);
		
		if (Health <= 0) {
			Health = 0;
			ZeroHealth();
		}
	}

	public abstract Team GetTeam();
}
