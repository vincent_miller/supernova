﻿using UnityEngine;
using System.Collections;

public class FixedEngine : MonoBehaviour {
    float v = 5;

    public void Update() {
        transform.parent.Translate(Vector3.back * v * Time.deltaTime);

        if (GlobalController.gameState == GameState.Won) {
            v *= Mathf.Pow(20, Time.deltaTime);
            

        }
    }
}
