﻿using UnityEngine;
using System.Collections;
using System;

public class FlipNormals : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Mesh m = GetComponent<MeshFilter> ().mesh;

		System.Array.Reverse (m.triangles);
		m.RecalculateNormals ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
