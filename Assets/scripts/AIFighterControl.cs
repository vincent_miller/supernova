/*using UnityEngine;
using System.Collections;

public class AIFighterControl : MonoBehaviour {

	private static float maxSpeed = 10;
	private static float acceleration = 10;

	protected Vector3 DestAngleAsLeader { get; set; }

	private Vector3 vmod;

	private Action orderAsLeader;

	protected Vector3 Destination { get; set; }

	protected Vector3 DesiredAngle {
		get {
			if (IsLeader) {
				return AvoidObstacles(DestAngleAsLeader.normalized);
			} else if (Leader.orderAsLeader == Action.Follow) {
				Fighter.Firing = false;
				Vector3 dest = -Leader.transform.forward;
				return AvoidObstacles(dest);
			} else {
				Fighter.Firing = Vector3.Dot (-transform.forward, (Leader.Target.transform.position - transform.position).normalized) > 0.75f;
				Vector3 dest = (Leader.Target.transform.position - transform.position).normalized;
				if (!Fighter.Firing) {
					dest = -Leader.transform.forward;
				}
				return AvoidObstacles(dest);
			}
		}
	}

	protected AIFighterControl Leader {
		get {
			return IsLeader ? this : squadParent.Leader;
		}
	}

	protected bool IsLeader {
		get {
			return squadParent == null;
		}
	}

	public Damageable Target { get; set; }

	public static Vector3[] rayPoints = new Vector3[] {
		new Vector3(-1 -1, 0), new Vector3(1, -1, 0),
		new Vector3(-1, 1, 0), new Vector3(1, 1, 0),
		new Vector3(0, 0, 0)
	};

	public AIFighterControl squadParent;

	protected Fighter Fighter {
		get {
			return GetComponent<Fighter> ();
		}
	}

	private Personal PickTarget() {
		return Find.NearestFilteredWith<Personal> (f => f.Team == Fighter.OtherTeam && Vector3.SqrMagnitude(transform.position - f.transform.position) < 200 * 200, transform.position);
	}

	void Start() {
		Destination = new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), 0);
	}

	// Update is called once per frame
	void Update () {
		if (GlobalController.Paused) {
			return;
		}

		if (!IsLeader && !squadParent.Fighter.Alive) {
			if (squadParent.squadParent != null) {
				squadParent = squadParent.squadParent;
			} else {
				foreach (AIFighterControl f in Find.AllWith<AIFighterControl>()) {
					if (f != this && f.squadParent == squadParent && f.Fighter.Alive) {
						squadParent = f;
						break;
					}
				}

				if (!squadParent.Fighter.Alive) {
					squadParent = null;
				}
			}

		}

		if (Target == null || !Target.Alive) {
			orderAsLeader = Action.Follow;
		}
		
		if (IsLeader && Fighter.Alive && !Fighter.Human) {

			float evadeNecessity = 0.0f;
	
			bool forceAttack = false;
			bool doEvade = false;
			Vector3 evadeVector = Vector3.zero;
			
			if (Target == null) {
				Target = PickTarget ();
			}

			Fighter[] enemies = Find.AllFilteredWith<Fighter> (f => f.Team == Fighter.OtherTeam && Vector3.SqrMagnitude (transform.position - f.transform.position) < 50 * 50);
			Personal[] all = Find.AllWith<Personal> ();

		
			foreach (Personal other in all) {
				if (other != Fighter) {
					AIFighterControl f = other.GetComponent<AIFighterControl>();
					if (f != null && f.Leader == Leader) {
						continue;
					}
					Vector3 toFighter = other.transform.position - transform.position;
				
					float e = 25 / toFighter.magnitude;
					if (e > evadeNecessity) {
						forceAttack = false;
						evadeNecessity = e;
						Vector3 cross1 = Vector3.Cross (transform.forward, toFighter);
						if (cross1.sqrMagnitude == 0.0f) {
							cross1 = Vector3.up;
						}
						doEvade = true;
						evadeVector = -toFighter;
					}
				}
			}
		
			evadeNecessity *= 2.0f - (Fighter.Health / Fighter.MaxHealth);

			if ((forceAttack || evadeNecessity < 5) && Target != null) {
				DestAngleAsLeader = (Target.transform.position - transform.position).normalized;
				Fighter.Firing = Vector3.Dot (-transform.forward, (Leader.Target.transform.position - transform.position).normalized) > 0.75f;
				orderAsLeader = Action.Attack;

					print ("Attack");
			} else if (false && doEvade) {
				DestAngleAsLeader = evadeVector;
				Fighter.Firing = false;
				orderAsLeader = Action.Follow;

					print ("Evade");
			} else if (Vector3.SqrMagnitude (transform.position - Destination) > 25) {
				DestAngleAsLeader = (Destination - transform.position).normalized;
				orderAsLeader = Action.Follow;
				Fighter.Firing = false;
					print ("Patrol");
			} else {
				Destination = new Vector3 (Random.Range (-50, 50), Random.Range (-50, 50), 0);
					print ("New Destination");
			}
		}

		vmod = Vector3.zero;
		if (!IsLeader && Fighter.Alive && !Fighter.Human) {
			Vector3 v = squadParent.transform.position - transform.position;
			vmod += (v.magnitude - 3) * v.normalized;

			float posOnLeaderZ = Vector3.Dot (-squadParent.transform.forward, transform.position - squadParent.transform.position);
			vmod += (posOnLeaderZ - 3) * squadParent.transform.forward;
			
			if (squadParent.squadParent != null) {
				Vector3 loll = Vector3.ProjectOnPlane (squadParent.transform.position - squadParent.squadParent.transform.position, -squadParent.squadParent.transform.forward);
				Vector3 mol = Vector3.ProjectOnPlane (transform.position - squadParent.transform.position, -squadParent.transform.forward);
				vmod += loll - mol;
			} else {
				foreach (AIFighterControl f in Find.AllWith<AIFighterControl>()) {
					if (f != this && f.squadParent == squadParent) {
						v = f.transform.position - transform.position;
						vmod += (v.magnitude - 6) * v.normalized;
					}
				}
			}
		}

		if (vmod.sqrMagnitude > 5 * 5) {
			vmod = vmod.normalized * 5;
		}
	}

	public void OnTakeDamage(float amount, Transform source) {
		Fighter f = source.GetComponent<Fighter> ();
		if (f != null) {
			Target = f;
		}
	}

	Vector3 AvoidObstacles(Vector3 destAngle) {
		foreach (Vector3 vec in rayPoints) {
			Vector3 p = transform.TransformPoint(vec);
			RaycastHit hit;
			if (Physics.Raycast(p, -transform.forward, out hit, 10) || Physics.Raycast(p, destAngle, out hit, 11)) {

				destAngle += hit.normal * 2;
				break;
			}
		}

		return destAngle;
	}

	void FixedUpdate() {
		if (Fighter.Alive && !Fighter.Human) {
			Rigidbody rb = GetComponent<Rigidbody> ();
		
			if (true) {
				transform.forward = Vector3.RotateTowards (transform.forward, -DesiredAngle, Time.fixedDeltaTime * 2, 0.0f);

				rb.velocity = -transform.forward * maxSpeed + vmod * 5;
			}
		}
	}

	private enum Action {
		Attack, Follow
	}
}
*/
using UnityEngine;
using System.Collections;

public class AIFighterControl : MonoBehaviour {

    private static float maxSpeed = 15;
    private static float acceleration = 10;

    protected Vector3 Destination { get; set; }
    protected Vector3 DestAngle { get; set; }
    public Damageable Target { get; set; }

    protected Fighter Fighter {
        get {
            return GetComponent<Fighter>();
        }
    }

    protected Carrier getCarrier() {
        return Find.FilteredWith<Carrier>(c => c.Team == Fighter.Team && c.Ready);
    }

    private Personal PickTarget() {
        return Find.NearestFilteredWith<Personal>(f => f.Team == Fighter.OtherTeam && Vector3.SqrMagnitude(transform.position - f.transform.position) < 200 * 200, transform.position);
    }

    void NewDest() {
        Destination = new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), Random.Range(-50, 50));
        
    }

    void Start() {
        NewDest();
    }

    // Update is called once per frame
    void Update() {
        if (GlobalController.Paused) {
            return;
        }

        if (!Fighter.Human && Fighter.Alive) {
            //if (!Fighter.Human) {

            float evade = 0.0f;

            bool attack = false;
            bool doEvade = false;
            Vector3 evadeVector = Vector3.zero;

            if (Target == null) {
                Target = PickTarget();
            }

            Fighter[] enemies = Find.AllFilteredWith<Fighter>(f => f.Team == Fighter.OtherTeam && Vector3.SqrMagnitude(transform.position - f.transform.position) < 50 * 50);
            Personal[] all = Find.AllWith<Personal>();


            foreach (Fighter enemy in enemies) {
                Vector3 fighterToThis = transform.position - enemy.transform.position;
                Vector3 thisToFighter = -fighterToThis;
                Vector3 cross = Vector3.Cross(fighterToThis.normalized, enemy.transform.forward);
                float e = 1.0f / Mathf.Max(Mathf.Deg2Rad * Vector3.Angle(fighterToThis, enemy.transform.forward), 0.1f);
                if (e > evade) {
                    if (Mathf.Deg2Rad * Vector3.Angle(thisToFighter, transform.forward) < 0.2f) {
                        attack = true;
                        Target = enemy;
                    } else {
                        attack = false;
                    }
                    evade = e;
                    doEvade = true;
                    evadeVector = Vector3.Cross(fighterToThis, cross);
                }
            }

            foreach (Personal other in all) {
                if (other != Fighter) {
                    Vector3 toFighter = other.transform.position - transform.position;

                    float e = 1000 / toFighter.sqrMagnitude;
                    if (e > evade) {
                        attack = false;
                        evade = e;
                        Vector3 cross1 = Vector3.Cross(transform.forward, toFighter);
                        if (cross1.sqrMagnitude == 0.0f) {
                            cross1 = Vector3.up;
                        }
                        doEvade = true;
                        evadeVector = -toFighter;
                        // evadeVector = Vector3.cross(f.transform.forward(),
                        // cross1);
                    }
                }
            }

            evade *= 2.0f - (Fighter.Health / Fighter.MaxHealth);

            Vector3 realDest = Destination;
            if (GlobalController.IsEscort) {
                Carrier c = getCarrier();
                if (c != null) {
                    realDest += getCarrier().transform.position;
                }
            }

            if ((attack || evade < 5) && Target != null) {
                DestAngle = Target.transform.position - transform.position;
                Fighter.Firing = true;
                if (Fighter.Human) {
                    //print ("Attack");
                }
            } else if (evade > 5 && doEvade) {
                DestAngle = evadeVector;
                Fighter.Firing = false;
                if (Fighter.Human) {
                    //print ("Evade");
                }
            } else if (Vector3.SqrMagnitude(transform.position - realDest) > 25) {
                DestAngle = realDest - transform.position;
                Fighter.Firing = false;
                if (Fighter.Human) {
                    //print ("Idle");
                }
            } else {
                NewDest();
                if (Fighter.Human) {
                    //print ("New Destination");
                }
            }
        }
    }

    void FixedUpdate() {

        if (!Fighter.Human) {
            DestAngle = DestAngle.normalized;

            Vector3 axis = Vector3.Cross(-transform.forward, DestAngle).normalized;
            float angleTo = Vector3.Angle(-transform.forward, DestAngle);
            float angle = Mathf.Clamp(angleTo, 30.0f, 360.0f) * Time.fixedDeltaTime;

            if (Vector3.Dot(-transform.forward, DestAngle) < -0.9f) {
                axis = transform.right;
                angle = Time.fixedDeltaTime * 360.0f;
            }
            //Quaternion rot = Quaternion.AngleAxis (angle, axis);

            //transform.rotation *= rot;

            transform.Rotate(axis, angle, Space.World);

            float dot = Mathf.Max(Vector3.Dot(-transform.forward, DestAngle.normalized), 0.0f);

            Rigidbody rb = GetComponent<Rigidbody>();

            rb.AddForce(-transform.forward * acceleration * dot * Time.fixedDeltaTime, ForceMode.Impulse);

            if (rb.velocity.sqrMagnitude > maxSpeed * maxSpeed) {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }
        }
    }
}