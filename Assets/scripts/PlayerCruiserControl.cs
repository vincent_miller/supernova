﻿using UnityEngine;
using System.Collections;

public class PlayerCruiserControl : MonoBehaviour {
	private int ControlIndex = 0;
    public bool DirectTurretMode = true;

	private Cruiser Cruiser {
		get {
			return GetComponent<Cruiser> ();
		}
	}
	
	public bool EnginesActive {
		get {
			return ControlIndex == 0 && Cruiser.Human;
		}
	}

	public void CheckOver (Turret turret) {
		int index = Cruiser.TurretIndex(turret);
		if (ControlIndex > index) {
			ControlIndex--;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalController.Paused) {
			return;
		}
		if (Cruiser.Human) {
            if (Input.GetKeyDown(KeyCode.X)) {
                DirectTurretMode = !DirectTurretMode;
                if (DirectTurretMode) {
                    for (int i = 0; i < Cruiser.NumTurrets; i++) {
                        Cruiser.TurretAt(i).Firing = false;
                    }
                }
            }

            if (EnginesActive && DirectTurretMode) {
                Camera cam = Cruiser.transform.FindChild("Camera").GetComponent<Camera>();
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                Vector3 target = ray.direction;
                bool point = false;

                if (Physics.Raycast(ray, out hit)) {
                    target = hit.point;
                    point = true;
                }

                for (int i = 0; i < Cruiser.NumTurrets; i++) {
                    Cruiser.TurretAt(i).DirectUpdate(target, !point);
                }
            }

			if (Input.GetKeyDown (KeyCode.Alpha1)) {
				ControlIndex--;
				if (ControlIndex < 0) {
					ControlIndex = Cruiser.NumTurrets;
				}
				UpdateCamera ();
			}

			if (Input.GetKeyDown (KeyCode.Alpha2)) {
				ControlIndex++;
				if (ControlIndex > Cruiser.NumTurrets) {
					ControlIndex = 0;
				}
				UpdateCamera ();
			}


			if (EnginesActive) {
				Cruiser.ShowCrosshair = false;
				UpdateThrusters ();
			} else {
				Cruiser.ShowCrosshair = true;
				Cruiser.TurretAt (ControlIndex - 1).PlayerUpdate ();
			}
		}
	}

	public void UpdateCamera() {
		Util.SetCameraEnabled (Cruiser.View, EnginesActive);

		for (int i = 0; i < Cruiser.NumTurrets; i++) {
			Turret turret = Cruiser.TurretAt(i);
			Util.SetCameraEnabled (turret.View, TurretActive (turret));
		}
	}

	public bool TurretActive(Turret turret) {
		if (ControlIndex == 0 || !Cruiser.Human) {
			return false;
		} else {
			return turret == Cruiser.TurretAt(ControlIndex - 1);
		}
	}

	public void UpdateThrusters() {
        if (Input.GetKey(KeyCode.W)) {
            Cruiser.UpdateTorque(Vector3.right, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.S)) {
            Cruiser.UpdateTorque(Vector3.left, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.A)) {
            Cruiser.UpdateTorque(Vector3.forward, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.D)) {
            Cruiser.UpdateTorque(Vector3.back, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.Q)) {
            Cruiser.UpdateTorque(Vector3.up, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.E)) {
            Cruiser.UpdateTorque(Vector3.down, 50f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.R)) {
            Cruiser.UpdateThrust(Vector3.down, 200f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.F)) {
            Cruiser.UpdateThrust(Vector3.up, 200f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.LeftShift)) {
            Cruiser.UpdateThrust(Vector3.forward, 200f);
            Cruiser.AIControl.ClearDirectives();
        } else if (Input.GetKey(KeyCode.LeftControl)) {
            Cruiser.UpdateThrust(Vector3.back, 200f);
            Cruiser.AIControl.ClearDirectives();
        } else if (!Cruiser.AIControl.HasDirectives()) {
            Cruiser.CancelThrust();
        }
	}
}
