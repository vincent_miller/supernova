using UnityEngine;
using System.Collections;

public class AIBomberControl : MonoBehaviour {

	private static float maxSpeed = 10;
	private static float acceleration = 10;

	protected Vector3 Destination { get; set; }
	protected Vector3 DestAngle { get; set; }
	protected Cruiser Target { get; set; }

	protected Bomber Bomber {
		get {
			return GetComponent<Bomber> ();
		}
	}
    protected Carrier getCarrier() {
        return Find.FilteredWith<Carrier>(c => c.Team == Bomber.Team && c.Ready);
    }

    private Cruiser PickTarget() {
		return Find.NearestFilteredWith<Cruiser> (f => f.Team == Bomber.OtherTeam && Vector3.SqrMagnitude(transform.position - f.transform.position) < 200 * 200, transform.position);
	}

	void Start() {
		Destination = new Vector3(Random.Range(-50, 50), Random.Range(-50, 50), 0);
	}

	// Update is called once per frame
	void Update () {
		if (GlobalController.Paused) {
			return;
		}
		
		if (!Bomber.Human && Bomber.Alive) {
			//if (!Bomber.Human) {

			float evade = 0.0f;
	
			bool attack = false;
			bool doEvade = false;
			Vector3 evadeVector = Vector3.zero;
			
			if (Target == null) {
				Target = PickTarget ();
			}

			Fighter[] enemies = Find.AllFilteredWith<Fighter> (f => f.Team == Bomber.OtherTeam && Vector3.SqrMagnitude (transform.position - f.transform.position) < 50 * 50);
			Personal[] all = Find.AllWith<Personal> ();


			foreach (Fighter enemy in enemies) {
				Vector3 fighterToThis = transform.position - enemy.transform.position;
				Vector3 cross = Vector3.Cross (fighterToThis.normalized, enemy.transform.forward);
				float e = 1.0f / Mathf.Max (Mathf.Deg2Rad * Vector3.Angle (fighterToThis, enemy.transform.forward), 0.1f);
				if (e > evade) {
					attack = false;
					evade = e;
					doEvade = true;
					evadeVector = Vector3.Cross (fighterToThis, cross);
				}
			}
		
			foreach (Personal other in all) {
				if (other != Bomber) {
					Vector3 toFighter = other.transform.position - transform.position;
				
					float e = 1000 / toFighter.sqrMagnitude;
					if (e > evade) {
						attack = false;
						evade = e;
						Vector3 cross1 = Vector3.Cross (transform.forward, toFighter);
						if (cross1.sqrMagnitude == 0.0f) {
							cross1 = Vector3.up;
						}
						doEvade = true;
						evadeVector = -toFighter;
						// evadeVector = Vector3.cross(f.transform.forward(),
						// cross1);
					}
				}
			}
		
			evade *= 2.0f - (Bomber.Health / Bomber.MaxHealth);

            Vector3 realDest = Destination;
            if (GlobalController.IsEscort) {
                Carrier c = getCarrier();
                if (c != null) {
                    realDest += getCarrier().transform.position;
                }
            }

            if ((attack || evade < 5) && Target != null) {
				DestAngle = Target.transform.position - transform.position;
				Bomber.Firing = false;
				RaycastHit hit;
				if (Physics.Raycast(new Ray(transform.position, -transform.forward), out hit)) {
					Bomber.Firing = hit.transform.GetComponent<Cruiser>() != null;
				}
				if (Bomber.Human) {
					//print ("Attack");
				}
			} else if (evade > 5 && doEvade) {
				DestAngle = evadeVector;
				Bomber.Firing = false;
				if (Bomber.Human) {
					//print ("Evade");
				}
			} else if (Vector3.SqrMagnitude (transform.position - realDest) > 25) {
				DestAngle = realDest - transform.position;
				Bomber.Firing = false;
				if (Bomber.Human) {
					//print ("Idle");
				}
			} else {
				Destination = new Vector3 (Random.Range (-50, 50), Random.Range (-50, 50), 0);
				if (Bomber.Human) {
					//print ("New Destination");
				}
			}
		}
	}

	void FixedUpdate() {
		
		if (!Bomber.Human) {
			DestAngle = DestAngle.normalized;

			Vector3 axis = Vector3.Cross (-transform.forward, DestAngle).normalized;
			float angleTo = Vector3.Angle (-transform.forward, DestAngle);
			float angle = Mathf.Clamp (angleTo, 30.0f, 360.0f) * Time.fixedDeltaTime;

			if (Vector3.Dot (-transform.forward, DestAngle) < -0.9f) {
				axis = transform.right;
				angle = Time.fixedDeltaTime * 360.0f;
			}
			//Quaternion rot = Quaternion.AngleAxis (angle, axis);

			//transform.rotation *= rot;

			transform.Rotate (axis, angle, Space.World);

			float dot = Mathf.Max (Vector3.Dot (-transform.forward, DestAngle.normalized), 0.0f);

			Rigidbody rb = GetComponent<Rigidbody> ();

			rb.AddForce (-transform.forward * acceleration * dot * Time.fixedDeltaTime, ForceMode.Impulse);
		
			if (rb.velocity.sqrMagnitude > maxSpeed * maxSpeed) {
				rb.velocity = rb.velocity.normalized * maxSpeed;
			}
		}
	}
}
