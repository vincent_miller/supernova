﻿using UnityEngine;
using System.Collections;

public class PlayerFighterControl : MonoBehaviour {

	private Personal Fighter {
		get {
			return GetComponent<Personal> ();
		}
	}

	// Use this for initialization
	void Start () {
	
	}

	void Update() {
		if (GlobalController.Paused) {
			return;
		}

		if (Fighter.Human && Fighter.Alive) {
			Fighter.Firing = Input.GetButton("Fire");
			
			Cursor.lockState = CursorLockMode.Confined;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GlobalController.Paused) {
			return;
		}

		if (Fighter.Human && Fighter.Alive) {
			GlobalController.ShowCursor = true;
			GlobalController.CurrentCursor = GlobalController.CursorTarget;
			Vector3 mouse;
			if (!GlobalController.Joystick) {
				mouse = (GlobalController.MouseCoord - new Vector2(Screen.width / 2, Screen.height / 2)) * Time.fixedDeltaTime;
			} else {
				mouse = new Vector3(Input.GetAxis("RX"), Input.GetAxis("RY")) * 4;
			}

			float Sensitivity = PlayerPrefs.GetFloat("Sensitivity", 1.0f) / 2;
            if (!Input.GetButton("Boost")) {

                transform.Rotate(Vector3.right, mouse.y * Sensitivity * 2);
                transform.Rotate(Vector3.up, mouse.x * Sensitivity * 2);

                transform.Rotate(Vector3.forward, Util.CombineAxes("Horizontal", "X") * 2);


                Rigidbody rb = GetComponent<Rigidbody>();

                rb.AddForce(transform.forward * Util.CombineAxes("Vertical", "Y") * -20 * Time.fixedDeltaTime, ForceMode.Impulse);

                if (rb.velocity.sqrMagnitude > 20 * 20)
                {
                    rb.velocity = rb.velocity.normalized * 20;
                }
            } else {
                transform.Rotate(Vector3.right, mouse.y * Sensitivity);
                transform.Rotate(Vector3.up, mouse.x * Sensitivity * 0.5f);

                transform.Rotate(Vector3.forward, Input.GetAxis("Horizontal") * 2);

                Rigidbody rb = GetComponent<Rigidbody>();

                rb.velocity = Vector3.MoveTowards(rb.velocity, transform.forward * -40, 40 * Time.fixedDeltaTime);
            }
        }
	}
}
