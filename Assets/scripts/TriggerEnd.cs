﻿using UnityEngine;
using System.Collections;

public class TriggerEnd : MonoBehaviour {
    private bool Won;
    private ExpirationTimer EndGame;

    // Update is called once per frame
    void Update () {
        if (Find.WithTeam<Ship>(Team.RED) == null) {
            GlobalController.gameState = GameState.Won;
        } else if ((GlobalController.IsEscort && Find.FilteredWith<Cruiser>(s => s.name == "BigShip") == null) || Find.WithTeam<Ship>(Team.BLUE) == null) {
            GlobalController.gameState = GameState.Lost;
        }

	}
}
