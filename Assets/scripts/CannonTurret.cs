﻿using UnityEngine;
using System.Collections;
using System;

public class CannonTurret : Turret {
	public GameObject Shell;
	private CooldownTimer FireTimer;
	private Vector3 ShellPosition = new Vector3(0.275f, 0, -1.07719f);

	// Use this for initialization
	new void Start() {
		DisplayName = "HE Cannon Turret";
		MaxHealth = 500;
        range = 150;
		base.Start ();

		FireTimer = new CooldownTimer (0.5f);
		TargetType = typeof(Cruiser);
	}
	
	new void Update() {
		base.Update();
		Explosion = GlobalController.ExplSmall;

		if (Firing && FireTimer.Use) {
			if (Cruiser.PlayerControl.TurretActive (this) || (Cruiser.Human && Cruiser.PlayerControl.DirectTurretMode)) {
				AudioSource.PlayClipAtPoint(GlobalController.SoundCannon, transform.position);
			}
			Vector3 pos = Barrels.TransformPoint (ShellPosition);
			ShellPosition.x *= -1;

			GameObject newShell = Instantiate(Shell);
			newShell.transform.position = pos;
			newShell.transform.forward = Barrels.forward;
			newShell.GetComponent<Rigidbody>().velocity = Barrels.forward * -50 + Cruiser.GetComponent<Rigidbody>().velocity;
            newShell.GetComponent<CannonShell>().Creator = Cruiser;
		}
	}
}
