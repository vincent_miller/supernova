
using System;
using System.Collections.Generic;
using UnityEngine;

public static class Find {
	public static T WithTeam<T> (Team team) where T : Ship {
		return FilteredWith<T> (s => s.Team == team);
	}
	
	public static T[] AllWithTeam<T> (Team team) where T : Ship {
		return AllFilteredWith<T> (s => s.Team == team);
	}
	
	public static T BestWithTeam<T> (Rater<T> rater, Team team) where T : Ship {
		return BestFilteredWith<T> (s => s.Team == team, rater);
	}
	
	public static T NearestWithTeam<T> (Team team, Vector3 position) where T : Ship {
		return NearestFilteredWith<T> (s => s.Team == team, position);
	}
	
	public static T With<T> () where T : MonoBehaviour {
		return FilteredWith<T> (c => true);
	}
	
	public static T[] AllWith<T> () where T : MonoBehaviour {
		return AllFilteredWith<T> (c => true);
	}
	
	public static T NearestWith<T> (Vector3 position) where T : MonoBehaviour {
		return NearestFilteredWith<T> (c => true, position);
	}
	
	public static T BestWith<T> (Rater<T> rater) where T : MonoBehaviour {
		return BestFilteredWith<T> (c => true, rater);
	}
	
	public static T NearestFilteredWith<T> (Predicate<T> filter, Vector3 position) where T : MonoBehaviour {
		return BestFilteredWith<T> (filter, c => 1.0f / Vector3.SqrMagnitude (c.transform.position - position));
	}
	
	public static T BestFilteredWith<T> (Predicate<T> filter, Rater<T> rater) where T : MonoBehaviour {
		T best = null;
		float bestval = float.NegativeInfinity;
		
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag ("Ship")) {
			T c = obj.GetComponent<T> ();
			if (c != null && filter(c)) {
				float f = rater(c);
				if (f > bestval) {
					bestval = f;
					best = c;
				}
			}
		}
		
		return best;
	}
	
	public static T FilteredWith<T> (Predicate<T> filter) where T : MonoBehaviour {
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag ("Ship")) {
			T c = obj.GetComponent<T> ();
			if (c != null && filter (c)) {
				return c;
			}
		}
		
		return null;
	}
	
	public static T[] AllFilteredWith<T> (Predicate<T> filter) where T : MonoBehaviour {
		List<T> items = new List<T> ();
		foreach (GameObject obj in GameObject.FindGameObjectsWithTag ("Ship")) {
			T c = obj.GetComponent<T> ();
			if (c != null && filter (c)) {
				items.Add(c);
			}
		}
		return items.ToArray ();
	}
	
	public delegate float Rater<T>(T item);
}


