using UnityEngine;
using System.Collections;

public class TimeToLive : MonoBehaviour {
	public float time = 10.0f;
	private ExpirationTimer timer;

	// Use this for initialization
	void Start () {
		timer = new ExpirationTimer(time);
		timer.Set();
	}
	
	// Update is called once per frame
	void Update () {
		if (timer.Expired) {
			Destroy(gameObject);
		}
	}
}

