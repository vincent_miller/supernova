using UnityEngine;
using System.Collections.Generic;
using System;

public static class Util {
	public static void Impel(GameObject entity, Vector3 force, Vector3 position) {
		Rigidbody rb = entity.GetComponent<Rigidbody> ();
		rb.AddForceAtPosition (force, position, ForceMode.Impulse);
	}

	public static void SetCameraEnabled(Camera camera, bool enable) {
		camera.enabled = enable;
		camera.GetComponent<AudioListener> ().enabled = enable;

		if (enable) {
			GlobalController.CurrentCamera = camera;
		}
	}

	public static Vector3 VectorToEuler(Vector3 dir) {
		dir = dir.normalized;
		float pitch = -Mathf.Rad2Deg * Mathf.Asin(dir.y);
		float yaw = Mathf.Rad2Deg * Mathf.Atan2(dir.x, dir.z);
		
		return new Vector3(pitch, yaw, 0);
	}

	public static float CombineAxes(params string[] axes) {
		float f = 0.0f;
		foreach (string axis in axes) {
			float val = Input.GetAxis(axis);
			if (Mathf.Abs(val) > Mathf.Abs(f)) {
				f = val;
			}
		}

		return f;
	}
	
	public static Vector2 StickAsMouse(string x, string y) {
		return new Vector3(Screen.width * (Input.GetAxis(x) * 0.5f + 0.5f), Screen.height * (Input.GetAxis(y) * 0.5f + 0.5f));
	}
}

