using UnityEngine;
using System.Collections;

public class Magazine {
	private ExpirationTimer reload;
	public int remainingShots { get; private set; }
	private int clipSize;

	public Magazine (int size, float reloadTime) {
		reload = new ExpirationTimer (reloadTime);
		remainingShots = size;
		this.clipSize = size;
	}

	public void Reload() {
		if (remainingShots < clipSize) {
			remainingShots = clipSize;
			reload.Set ();
		}
	}

	public bool Reloading() {
		return !reload.Expired;
	}

	public bool Fire () {
		if (remainingShots > 0 && reload.Expired) {
			remainingShots--;

			if (remainingShots == 0) {
				Reload();
			}

			return true;
		} else {
			return false;
		}
	}
}

