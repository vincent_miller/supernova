﻿using UnityEngine;
using System.Collections;

public class Bomber : Personal {
	private CooldownTimer Shoot;
	private Vector3 Side = new Vector3 (0.3f, 0, -1);

	public GameObject Rocket;
	public Magazine mag;
	public AudioClip ShootSound;

	public Texture2D RocketImage;


	// Use this for initialization
	new void Start () {
		base.Start ();
		mag = new Magazine (7, 3.0f);
		gameObject.AddComponent<PlayerFighterControl> ();
		gameObject.AddComponent<AIBomberControl> ();

		Shoot = new CooldownTimer (0.25f);

		ShowCrosshair = true;
	}

	new void OnGUI() {
		base.OnGUI ();
		if (Human && !mag.Reloading()) {
			for (int i = 0; i < mag.remainingShots; i++) {
				GUI.DrawTexture(new Rect (50, Screen.height - 200 - (i * 32), 64, 32), RocketImage);
			}
		}
	}
	

	// Update is called once per frame
	new void Update () {
		if (GlobalController.Paused) {
			return;
		}

		base.Update ();

		if (Human && Alive) {
			if (Input.GetKeyDown (KeyCode.R)) {
				mag.Reload ();
			}
		}

		if (Firing && Alive && Shoot.Use && mag.Fire ()) {

			Ray ray;

			if (Human) {
				ray = FirstPersonView.ScreenPointToRay(GlobalController.MouseCoord);
				AudioSource.PlayClipAtPoint(ShootSound, transform.position);
			} else {
				Vector3 shot = -transform.forward;

				shot = Quaternion.Euler(Random.Range(-4, 4), Random.Range(-4, 4), 0) * shot;

				ray = new Ray(transform.position, shot);
			}

			GameObject newTracer = Instantiate(Rocket);
			newTracer.transform.position = transform.TransformPoint(Side);
			newTracer.transform.forward = ray.direction;
            newTracer.GetComponent<Rocket>().Creator = this;
			newTracer.GetComponent<Rocket>().Velocity = ray.direction * 50;

			Side.x *= -1;
		}
	}
}
