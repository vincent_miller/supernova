using UnityEngine;
using System.Collections;

public class AIMove : CruiserDirective {
	private Vector3 point;
	
	public AIMove(Vector3 point) {
		this.point = point;
	}

	public void Start(Cruiser ship) {
	}

	private Vector3 getProjectedDirection(Ship ship) {
		return Vector3.ProjectOnPlane (point - ship.transform.position, ship.transform.right);
	}

	public void Update(Cruiser ship) {
		Vector3 p = getProjectedDirection(ship).normalized;

		ship.UpdateThrust(ship.transform.InverseTransformVector(-p), 200.0f);
	}

	public bool Done(Cruiser ship) {
		Vector3 p = getProjectedDirection(ship);
		
		return p.sqrMagnitude < 9;
	}
}

