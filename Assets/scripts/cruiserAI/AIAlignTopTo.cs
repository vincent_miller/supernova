using UnityEngine;
using System.Collections;

public class AIAlignTopTo : CruiserDirective {
	protected Vector3 destAngle;
	
	public AIAlignTopTo(Vector3 angle) {
		this.destAngle = angle.normalized;
	}

	public void Start(Cruiser ship) {
	}

	public virtual void Update(Cruiser ship) {
		Vector3 up = ship.transform.up;
        Vector3 proj = Vector3.ProjectOnPlane(destAngle, ship.transform.forward);

        Vector3 cross = ship.transform.InverseTransformDirection(Vector3.Cross(up, proj).normalized);

        
		ship.UpdateTorque(cross, 50.0f);
	}

	public bool Done(Cruiser ship) {
		Vector3 up = ship.transform.up;
        Vector3 proj = Vector3.ProjectOnPlane(destAngle, ship.transform.forward).normalized;
        float dot = Vector3.Dot(up, proj);

        return Mathf.Abs(dot) > 0.8f;
	}
}

