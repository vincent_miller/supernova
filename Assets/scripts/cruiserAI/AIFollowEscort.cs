using UnityEngine;
using System.Collections;

public class AIFollowEscort : CruiserDirective {
    private Vector3 point;
    private Cruiser Escort;
    private CruiserDirective subDirective = null;
    private bool approach;
    private bool moveAway;

    private float followThrust = 200f;

    public void Start(Cruiser ship) {
        Escort = Find.FilteredWith<Cruiser>(s => s.name == "BigShip");
    }


    public void Update(Cruiser ship) {

        if (Escort == null) {
            return;
        }
        if (subDirective != null) {
            subDirective.Update(ship);
            if (subDirective.Done(ship)) {
                subDirective = null;
            }
        } else {

            Vector3 inTargetSpace = Escort.transform.InverseTransformPoint(ship.transform.position);

            float dot = Vector3.Dot(ship.transform.forward, Escort.transform.forward);
            Vector3 targetToThis = ship.transform.position - Escort.transform.position;



            if (Mathf.Abs(dot) < 0.8f) {
                subDirective = new AIRotateTo(Escort.transform.forward);
                subDirective.Start(ship);
            } else {
                Vector3 targetToThisProj = Vector3.ProjectOnPlane(targetToThis, ship.transform.forward);
                
                if (targetToThisProj.sqrMagnitude > 150 * 150 && !(approach || moveAway)) {
                    subDirective = new AIAlignTopTo(targetToThis);
                    subDirective.Start(ship);
                    approach = true;
                } else if (targetToThisProj.sqrMagnitude < 25 * 25 && !(approach || moveAway)) {
                    subDirective = new AIAlignTopTo(targetToThis);
                    subDirective.Start(ship);
                    moveAway = true;
                } else if (approach) {
                    Vector3 desiredThrust = Vector3.forward;
                    desiredThrust -= ship.transform.InverseTransformPoint(Escort.transform.position) * 0.01f;
                    ship.UpdateThrust(desiredThrust.normalized, 200.0f);
                    if (targetToThisProj.sqrMagnitude < 50 * 50) {
                        approach = false;
                        subDirective = new AIRotateTo(Escort.transform.forward);
                        subDirective.Start(ship);
                    }
                } else if (moveAway) {
                    Vector3 desiredThrust = Vector3.forward;
                    desiredThrust += ship.transform.InverseTransformPoint(Escort.transform.position) * 0.01f;
                    ship.UpdateThrust(desiredThrust.normalized, 200.0f);
                    if (targetToThisProj.sqrMagnitude > 50 * 50) {
                        moveAway = false;
                        subDirective = new AIRotateTo(Escort.transform.forward);
                        subDirective.Start(ship);
                    }
                } else if (Escort.Team != ship.Team && Vector3.Dot(ship.transform.up, -targetToThisProj.normalized) < 0.7) {
                    subDirective = new AIAlignTopTo(targetToThis);
                    subDirective.Start(ship);
                } else {
                    if (inTargetSpace.z < -100) {
                        followThrust = 50f;
                    } else if (inTargetSpace.z > 100) {
                        followThrust = 200f;
                    } else {
                        float f = Vector3.Dot(ship.GetComponent<Rigidbody>().velocity, -Escort.transform.forward);
                        float g = 5;
                        if (f > g) {
                            followThrust -= 0.1f;
                        } else {
                            followThrust += 0.1f;
                        }
                    }

                    if (dot > 0) {
                        ship.UpdateThrust(Vector3.forward, followThrust);
                    } else {
                        ship.UpdateThrust(Vector3.back, followThrust);
                    }
                }
            }

        }
    }

    public bool Done(Cruiser ship) {
        return Escort == null;
    }
}

