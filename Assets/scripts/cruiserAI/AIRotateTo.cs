using UnityEngine;
using System.Collections;

public class AIRotateTo : CruiserDirective {
	protected Vector3 destAngle;
	protected Vector3 axis;
	protected bool mode = false;
	
	public AIRotateTo(Vector3 angle) {
		this.destAngle = angle.normalized;
		this.axis = Vector3.zero;
	}

	public void Start(Cruiser ship) {
		mode = false;
	}

	public virtual void Update(Cruiser ship) {
		Vector3 forward;
		if (Vector3.Dot(-ship.transform.forward, destAngle) > 0) {
			forward = -ship.transform.forward;
		} else {
			forward = ship.transform.forward;
		}
		
		Vector3 cross = ship.transform.InverseTransformVector(Vector3.Cross(forward, destAngle));
		
		if (!mode && Mathf.Abs(cross.y) < 0.1f) {
			mode = true;
		}
		
		if (mode) {
			if (cross.x > 0) {
				axis = Vector3.right;
			} else {
				axis = Vector3.left;
			}
		} else {
			if (cross.y > 0) {
				axis = Vector3.up;
			} else {
				axis = Vector3.down;
			}
		}

		ship.UpdateTorque(-axis, 50.0f);
	}

	public bool Done(Cruiser ship) {
		if (!mode) {
			return false;
		}
		
		Vector3 forward;
		if (Vector3.Dot(-ship.transform.forward, destAngle) > 0) {
			forward = -ship.transform.forward;
		} else {
			forward = ship.transform.forward;
		}
		
		Vector3 cross = ship.transform.InverseTransformVector(Vector3.Cross(forward, destAngle));
		
		return Mathf.Abs(cross.x) < 0.1f;
	}
}

