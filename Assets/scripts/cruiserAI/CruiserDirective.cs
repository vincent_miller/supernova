using UnityEngine;
using System.Collections;

public interface CruiserDirective {

	void Start(Cruiser ship);
	void Update(Cruiser ship);
	bool Done(Cruiser ship);
}

