using UnityEngine;
using System.Collections;

public class AIMoveTo : CruiserDirective {
	private AILookAt rotation;
	private AIMove movement;
	private bool turning = true;
	
	public AIMoveTo(Vector3 point) {
		rotation = new AILookAt(point);
		movement = new AIMove(point);
	}

	public void Start(Cruiser ship) {
		rotation.Start(ship);
		movement.Start(ship);
	}

	public void Update(Cruiser ship) {
		if (rotation.Done(ship)) {
			turning = false;
			movement.Update(ship);
		} else {
			if (!turning) {
				turning = true;
				rotation.Start(ship);
			}
			rotation.Update(ship);
		}
	}

	public bool Done(Cruiser ship) {
		return rotation.Done(ship) && movement.Done(ship);
	}
}
