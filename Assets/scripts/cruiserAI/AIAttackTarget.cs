using UnityEngine;
using System.Collections;

public class AIAttackTarget : CruiserDirective {
	private AIMoveTo moveInRange;
	private AILookTopAt faceTarget;
	private Ship target;
	private bool moving;
	
	public AIAttackTarget(Ship target) {
		this.target = target;
	}

	private bool InRange(Ship ship) {
		return Vector3.SqrMagnitude(target.transform.position - ship.transform.position) < 45 * 45;
	}

	private bool InAim(Ship ship) {
		return Vector3.Dot(ship.transform.up, Vector3.Normalize(target.transform.position - ship.transform.position)) > 0.5;
	}

	private Vector3 TargetPosition(Ship ship) {
		if (InRange(ship)) {
			return ship.transform.position;
		} else {
			Vector3 toMe = ship.transform.position - target.transform.position;
			return target.transform.position + (toMe.normalized * 45);
		}

	}
	
	public void Start(Cruiser ship) {
		if (target == null) {
			return;
		}

		moving = !InRange(ship);
		moveInRange = new AIMoveTo(TargetPosition(ship));
		faceTarget = new AILookTopAt(ship.transform.position);
	}
	
	public void Update(Cruiser ship) {
		if (target == null) {
			return;
		}

		if (InRange(ship)) {
			moving = false;
			faceTarget.Update(ship);

			if (faceTarget.Done(ship) && !InAim(ship)) {
				faceTarget = new AILookTopAt(target.transform.position);
				faceTarget.Start(ship);
			}
		} else {
			if (!moving) {
				moving = true;
				moveInRange.Start(ship);
			}
			moveInRange.Update(ship);

			if (moveInRange.Done(ship) && !InRange(ship)) {
				moveInRange = new AIMoveTo(TargetPosition(ship));
			}
		}

		if (InRange(ship)) {
			for (int i = 0; i < ship.NumTurrets; i++) {
				Turret turret = ship.TurretAt(i);
				if (turret.CanTarget(target)) {
					turret.Target = target;
				}
			}
		}
	}
	
	public bool Done(Cruiser ship) {
		if (target == null) {
			return true;
		}
		return InRange(ship) && InAim(ship);
	}
}

