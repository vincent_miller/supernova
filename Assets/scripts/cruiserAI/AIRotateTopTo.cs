using UnityEngine;
using System.Collections;

public class AIRotateTopTo : CruiserDirective {
	protected Vector3 destAngle;
	protected Vector3 axis;
	protected bool mode = false;
	
	public AIRotateTopTo(Vector3 angle) {
		this.destAngle = angle.normalized;
		this.axis = Vector3.zero;
	}

	public void Start(Cruiser ship) {
		mode = false;
	}

	public virtual void Update(Cruiser ship) {
		Vector3 up = ship.transform.up;
		
		Vector3 cross = ship.transform.InverseTransformVector(Vector3.Cross(up, destAngle));

		if (!mode && Mathf.Abs(cross.x) < 0.1f) {
			mode = true;
		}
		
		if (mode) {
			if (cross.z > 0) {
				axis = Vector3.forward;
			} else {
				axis = Vector3.back;
			}
		} else {
			if (cross.x > 0) {
				axis = Vector3.left;
			} else {
				axis = Vector3.right;
			}
		}

		ship.UpdateTorque(axis, 50.0f);
	}

	public bool Done(Cruiser ship) {
		if (!mode) {
			return false;
		}
		
		Vector3 up = ship.transform.up;
		Vector3 cross = ship.transform.InverseTransformVector(Vector3.Cross(up, destAngle));
		
		return Mathf.Abs(cross.z) < 0.1f;
	}
}

