using UnityEngine;
using System.Collections;

public class AILookAt : AIRotateTo {
	private Vector3 point;
	
	public AILookAt(Vector3 point) : base(Vector3.zero) {
		this.point = point;
	}

	public override void Update(Cruiser ship) {
		destAngle = getAngle(ship);
		base.Update(ship);
	}
	
	private Vector3 getAngle(Cruiser ship) {
		return (point - ship.transform.position).normalized;
	}
}

