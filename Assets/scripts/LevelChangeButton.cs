﻿using UnityEngine;
using System.Collections;

public class LevelChangeButton : MonoBehaviour {
	public GUISkin skin;

	void Start() {
		Camera.main.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat ("MusicVolume", 0.5f);
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		GUI.skin = skin;
		if (GUI.Button(new Rect(Screen.width / 2 - 100, 100, 200, 50), "Deathmatch")) {
			Application.LoadLevel ("Deathmatch");
		}

		if (GUI.Button(new Rect(Screen.width / 2 - 100, 175, 200, 50), "Escort")) {
			Application.LoadLevel ("Escort");
		}

		if (GUI.Button(new Rect(Screen.width / 2 - 100, 250, 200, 50), "Exit Game")) {
			Application.Quit ();
		}
	}
}
