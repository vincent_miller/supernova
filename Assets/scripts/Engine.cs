﻿using UnityEngine;
using System.Collections;

public class Engine : Damageable {
	protected Cruiser Cruiser;
	protected ParticleSystem Exhaust;

	public bool Ready { get; protected set; }

	// Use this for initialization
	new void Start () {
		DisplayName = "Thruster";
		MaxHealth = 2500;
		base.Start();
		Cruiser = transform.parent.GetComponent<Cruiser> ();
		Exhaust = transform.FindChild ("Exhaust").GetComponent<ParticleSystem> ();
		Exhaust.enableEmission = false;
	}
    

	public virtual void UpdateThrust(Vector3 desiredDirection, float t) {
		desiredDirection.x = 0;
		desiredDirection.Normalize();
		Vector3 forward = transform.localRotation * Vector3.forward;
		Vector3 cross = Vector3.Cross (forward, desiredDirection);

		cross.y = 0;
		cross.z = 0;

		cross.Normalize ();

		float dot = Vector3.Dot (forward, desiredDirection);
		float angle = Vector3.Angle (forward, desiredDirection);

		if (cross.sqrMagnitude < 0.001f) {
			if (dot < 0) {
				cross = Vector3.left;
				angle = 180;
			} else {
				angle = 0;
			}
		} else {
			cross.Normalize();
		}

		transform.Rotate (cross, Mathf.Min (angle, 360 * Time.deltaTime));
		
		forward = transform.localRotation * Vector3.forward;
		
		float thrust = Mathf.Max(Vector3.Dot(forward, desiredDirection), 0) * t;
		if (thrust > 0.99f * t) {
			Ready = true;
		} else {
			Ready = false;
		}

		if (Cruiser.ThrustReady ()) {
			Util.Impel (transform.parent.gameObject, (transform.rotation * Vector3.back) * thrust * Time.deltaTime, transform.position);
			Exhaust.enableEmission = true;
		} else {
			Exhaust.enableEmission = false;
		}
	}
	
	public override Team GetTeam () {
		return Cruiser.Team;
	}

	public void CancelThrust() {
		Exhaust.enableEmission = false;
	}

	public void UpdateTorque(Vector3 desiredTorque, float t) {
		Vector3 perfectDirection = Vector3.Cross(desiredTorque, transform.localPosition);
		UpdateThrust(perfectDirection, t);
	}

	public override void ZeroHealth() {
		Destroy();
	}
	
	public override void Destroy() {
		base.Destroy ();
		Cruiser.UpdateTurretList ();
	}
}
