﻿using UnityEngine;
using System.Collections.Generic;

public class TacticalView : MonoBehaviour {
	public static LinkedList<Cruiser> Selected;

	// Use this for initialization
	void Start () {
		Selected = new LinkedList<Cruiser> ();
	}

	void ClearSelection() {
		foreach (Cruiser c in Selected) {
			c.Selected = false;
		}
		Selected.Clear();
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalController.ControlObject == null) {
			GlobalController.ShowCursor = true;
			if (Input.GetMouseButton (1)) {
				transform.RotateAround (transform.position, transform.right, -Input.GetAxis ("Mouse Y"));
				transform.RotateAround (transform.position, Vector3.up, Input.GetAxis ("Mouse X"));
			}

			transform.Translate (Vector3.right * Input.GetAxis("Horizontal") * 5);
			transform.Translate (Vector3.up * Input.GetAxis("Vertical") * 5);
			transform.Translate (Vector3.forward * Input.GetAxis("Mouse ScrollWheel") * 100);

			if (Input.GetKeyDown (KeyCode.C) && Selected.Count > 0) {
				GlobalController.AssumeControl(Selected.Last.Value);
			}

			if (Input.GetMouseButtonDown (0)) {
				RaycastHit hit;
				if (Physics.Raycast(GetComponent<Camera>().ScreenPointToRay(GlobalController.MouseCoord), out hit)) {
					Cruiser c = hit.transform.GetComponent<Cruiser>();
					if (c != null) {
						if (c.Team == Team.BLUE) {
							if (!Input.GetKey(KeyCode.LeftShift)) {
								ClearSelection();
							}
							Selected.AddLast(c);
							c.Selected = true;
						} else {
							if (Selected.Count > 0) {
								foreach (Cruiser cr in Selected) {
									if (!Input.GetKey(KeyCode.LeftShift)) {
										cr.AIControl.ClearDirectives();
									}
									cr.AIControl.AddDirective(new AIAttackTarget(c));
								}
							}
						}
					}
				} else {
					Plane xz = new Plane(Vector3.up, Vector3.zero);
					float f;
					Ray ray = GlobalController.SpawnCamera.ScreenPointToRay(GlobalController.MouseCoord);
					if (xz.Raycast(ray, out f)) {
						Vector3 v = ray.origin + ray.direction * f;
						foreach (Cruiser cr in Selected) {
							if (!Input.GetKey(KeyCode.LeftShift)) {
								cr.AIControl.ClearDirectives();
							}
							cr.AIControl.AddDirective(new AIMoveTo(v));
						}
					}
				}
			}
			GlobalController.CurrentCursor = GlobalController.CursorDefault;
			RaycastHit hit2;
			
			if (Physics.Raycast(GlobalController.SpawnCamera.ScreenPointToRay(GlobalController.MouseCoord), out hit2)) {
				GameObject target = hit2.transform.gameObject;
				Cruiser cruiser = target.GetComponent<Cruiser>();
				if (cruiser != null) {
					if (cruiser.Team == Team.BLUE) {
						GlobalController.CurrentCursor = GlobalController.CursorSelect;
					} else {
						GlobalController.CurrentCursor = GlobalController.CursorTarget;
					}
				}
			}
		}
	}
}
