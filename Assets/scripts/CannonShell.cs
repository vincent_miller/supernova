﻿using UnityEngine;
using System.Collections;

public class CannonShell : MonoBehaviour {
	public float Damage = 150;
	public GameObject Explosion;
    public Ship Creator;
	private Collider start;

    public GameObject Smoke;

    void Start () {
		GameObject[] shields = GameObject.FindGameObjectsWithTag ("Shield");
		foreach (GameObject obj in shields) {
			if (Vector3.SqrMagnitude (obj.transform.position - transform.position) < 20.4f * 20.4f) {
				start = obj.GetComponent<Collider> ();
			}
		}
	}

	void OnTriggerEnter(Collider col) {
		if (col == start) {
			return;
		}
		Damageable ship = col.gameObject.GetComponentInParent<Damageable> ();
		if (ship != null) {
			ship.Damage (Damage, Creator != null ? Creator.transform : transform);

            if (Random.value < 0.1f) {
                GameObject obj = Instantiate(Smoke);
                obj.transform.parent = ship.transform;
                obj.transform.position = transform.position;
                obj.transform.forward = -GetComponent<Rigidbody>().velocity;
            }

            if (Creator != null && Creator.Human) {
				GlobalController.ShowHitMarker.Set();
			}
		}
		Instantiate (Explosion).transform.position = transform.position;
		Destroy (gameObject);
	}
}
