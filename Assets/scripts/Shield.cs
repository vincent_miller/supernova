﻿using UnityEngine;
using System.Collections;

public class Shield : Damageable {

	// Use this for initialization
	new void Start () {
		DisplayName = "Bound Particle Field";
		MaxHealth = 25000;
		base.Start();
	}

	new void Update() {
		base.Update();
		Health = Mathf.Min (MaxHealth, Health + Time.deltaTime * 50);
	}
	
	public override void ZeroHealth() {
		Destroy();
	}

	public override void Destroy() {
        transform.parent.FindChild("ShieldBurst").GetComponent<ParticleSystem>().Play();
		Destroy (gameObject);
	}

	public override Team GetTeam() {
		return GetComponentInParent<Ship>().Team;
	}
}
