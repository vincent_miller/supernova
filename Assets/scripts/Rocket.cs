﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {
	public GameObject Explosion;
    public Ship Creator;
	public Vector3 Velocity;

	public float Damage = 100;
	private Collider start;

    public GameObject Smoke;

	// Use this for initialization
	void Start () {
		GameObject[] shields = GameObject.FindGameObjectsWithTag ("Shield");
		foreach (GameObject obj in shields) {
			if (Vector3.SqrMagnitude (obj.transform.position - transform.position) < 20.4f * 20.4f) {
				start = obj.GetComponent<Collider> ();
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position += Velocity * Time.fixedDeltaTime;
    }

    void OnTriggerEnter(Collider col) {
		if (col == start) {
			return;
        }
        


        GameObject exp = Instantiate (Explosion);
		exp.transform.position = transform.position;

		Damageable ship = col.gameObject.GetComponentInParent<Damageable> ();
		if (ship != null) {
			ship.Damage (Damage, Creator != null ? Creator.transform : transform);

            if (Random.value < 0.1f) {
                GameObject obj = Instantiate(Smoke);
                obj.transform.parent = ship.transform;
                obj.transform.position = transform.position;
                obj.transform.forward = -Velocity;
            }

            if (Creator != null && Creator.Human) {
				GlobalController.ShowHitMarker.Set();
			}
		}


        Destroy (gameObject);
	}
}
