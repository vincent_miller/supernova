﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Reflection;

public class Turret : Damageable {
    public Ship Target { get; set; }
    public Type TargetType;
    public float range = 50;

    protected Transform Body {
        get {
            return transform;
        }
    }

    public override Team GetTeam() {
        return Cruiser.Team;
    }

    protected Transform Barrels {
        get {
            return Body.FindChild("Barrels");
        }
    }

    protected Cruiser Cruiser {
        get {
            return transform.parent.GetComponent<Cruiser>();
        }
    }

    public bool UpsideDown;

    public Camera View {
        get {
            return Barrels.FindChild("Camera").GetComponent<Camera>();
        }
    }

    public bool Firing { get; set; }

    protected bool AimingAtFriendly {
        get {
            RaycastHit hit;

            if (Physics.Raycast(new Ray(Barrels.position, -Barrels.forward), out hit)) {

                Ship ship = hit.transform.GetComponent<Ship>();
                return ship != null && ship.Team == Cruiser.Team;
            } else {
                return false;
            }
        }
    }

    private Ship PickTarget() {
        float min = float.PositiveInfinity;
        Ship best = null;

        /*
        if (TargetType == typeof(Cruiser)) {
            best = Find.FilteredWith<Cruiser>(s => s.name == "BigShip" && s.Team != Cruiser.Team);
            if (best != null) {
                return best;
            }
        }*/

        foreach (GameObject o in GameObject.FindGameObjectsWithTag("Ship")) {
            Ship s = o.GetComponent<Ship>();
            if (s != null) {
                float d = Vector3.SqrMagnitude(s.transform.position - transform.position);
                if (s.name == "BigShip") {
                    d /= 4;
                }
                if (TargetType.IsInstanceOfType(s) && CanTarget(s) && d < min) {
                    best = s;
                    min = d;
                }
            }
        }

        return best;
    }

    public void TryTarget(Ship s) {
        if (TargetType.IsInstanceOfType(s) && CanTarget(s)) {
            Target = s;
        }
    }

    public bool CanTarget(Ship f) {
        float distance = Vector3.SqrMagnitude(transform.position - f.transform.position);

        if (f.name == "BigShip") {
            distance /= 4;
        }

        Vector3 lpos = transform.InverseTransformPoint(f.transform.position);
        return (f.Team == Cruiser.OtherTeam) && (distance < range * range) && ((UpsideDown && lpos.y < 0) || (!UpsideDown && lpos.y > 0));
    }

    public bool CanTarget(Vector3 v) {
        Vector3 ldir = transform.InverseTransformVector(v);
        return (UpsideDown && ldir.y < 0) || (!UpsideDown && ldir.y > 0);
    }

    public void DirectUpdate(Vector3 target, bool dir) {
        if (!dir) {
            target -= transform.position;
        }

        if (CanTarget(target)) {
            Body.localEulerAngles = Vector3.zero;
            Vector3 local = -transform.InverseTransformVector(target);
            Vector3 angles = Util.VectorToEuler(local);
            Body.localEulerAngles = new Vector3(0, angles.y, 0);
            Barrels.localEulerAngles = new Vector3(angles.x, 0, 0);

            RaycastHit hit;
            Firing = Input.GetMouseButton(0);
            if (Physics.Raycast(new Ray(Barrels.transform.position, -Barrels.transform.forward), out hit)) {
                if(hit.collider.GetComponentInParent<Ship>() == Cruiser) {
                    Firing = false;
                }
            }

        } else {
            Firing = false;
        }
    }

    public void AIUpdate() {
        if (Target == null) {
            Target = PickTarget();
        }

        if (Target != null && CanTarget(Target)) {
            float curYaw = Body.localEulerAngles.y;
            float curPitch = Barrels.localEulerAngles.x;


            Body.localEulerAngles = Vector3.zero;
            Barrels.localEulerAngles = Vector3.zero;
            Vector3 localPoint = -Barrels.InverseTransformPoint(Target.transform.position);
            Vector3 angles = Util.VectorToEuler(localPoint);

            float dYaw = Mathf.DeltaAngle(curYaw, angles.y);
            float dPitch = Mathf.DeltaAngle(curPitch, angles.x);

            if (dYaw > 50f * Time.deltaTime) {
                angles.y = Mathf.LerpAngle(curYaw, angles.y, 50f * Time.deltaTime / dYaw);
            }

            if (dPitch > 25f * Time.deltaTime) {
                angles.x = Mathf.LerpAngle(curPitch, angles.x, 25f * Time.deltaTime / dPitch);
            }

            Body.localEulerAngles = new Vector3(0, angles.y, 0);
            Barrels.localEulerAngles = new Vector3(angles.x, 0, 0);

            Firing = !AimingAtFriendly;
        } else {
            Firing = false;
        }
    }



    // Update is called once per frame
    public void PlayerUpdate() {

        RaycastHit hit;
        if (Physics.Raycast(new Ray(Barrels.position, -Barrels.forward), out hit)) {
            Damageable s = hit.collider.GetComponentInParent<Damageable>();
            if (s != null) {
                GlobalController.Targetting = s;
            }
        }

        Cursor.lockState = CursorLockMode.Locked;
        GlobalController.ShowCursor = false;

        Body.Rotate(0, Input.GetAxis("Mouse X"), 0);

        Vector3 angles = Barrels.localEulerAngles;

        angles.x += Input.GetAxis("Mouse Y");

        if (UpsideDown) {
            if (angles.x > 0 && angles.x < 180) {
                angles.x = 0;
            }

            if (angles.x < 270 && angles.x >= 180) {
                angles.x = 270;
            }
        } else {
            if (angles.x > 90) {
                angles.x = 90;
            }

            if (angles.x < 0) {
                angles.x = 0;
            }
        }

        Barrels.localEulerAngles = angles;

        Firing = Input.GetMouseButton(0);
    }

    public override void ZeroHealth() {
        Destroy();
    }

    public override void Destroy() {
        Cruiser.PlayerControl.CheckOver(this);

        base.Destroy();
        Cruiser.UpdateTurretList();
    }
}
