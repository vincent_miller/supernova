﻿using UnityEngine;
using System.Collections;

public class Fighter : Personal {

	private CooldownTimer Shoot;

	public GameObject Tracer;

	public AudioClip ShootSound;

	public Fighter parent;


	// Use this for initialization
	new void Start () {
		base.Start ();
		
		gameObject.AddComponent<PlayerFighterControl> ();
		//gameObject.AddComponent<AIFighterControl> ();

		Shoot = new CooldownTimer (0.1f);

		ShowCrosshair = true;
	}

	protected override void OnTakeDamage(float amount, Transform source) {
		//GetComponent<AIFighterControl> ().OnTakeDamage (amount, source);
	}
	
	// Update is called once per frame
	new void Update () {
		if (GlobalController.Paused) {
			return;
		}

		base.Update ();

		if (Firing && Alive && Shoot.Use) {

			RaycastHit hit;

			Ray ray;

			if (Human) {
				ray = FirstPersonView.ScreenPointToRay(GlobalController.MouseCoord);
				AudioSource.PlayClipAtPoint(ShootSound, transform.position);
			} else {
				Vector3 shot = -transform.forward;

				shot = Quaternion.Euler(Random.Range(-3, 3), Random.Range(-3, 3), 0) * shot;

				ray = new Ray(transform.position, shot);
			}

			GameObject newTracer = Instantiate(Tracer);
			newTracer.transform.position = transform.TransformPoint(new Vector3(0.15f, 0, 0)) + ray.direction / 2;
			newTracer.transform.forward = ray.direction;
			newTracer.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity + ray.direction * 100;
			newTracer.GetComponent<Rigidbody>().detectCollisions = false;

			newTracer = Instantiate(Tracer);
			newTracer.transform.position = transform.TransformPoint(new Vector3(-0.15f, 0, 0)) + ray.direction / 2;
			newTracer.transform.forward = ray.direction;
			newTracer.GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity + ray.direction * 100;
			newTracer.GetComponent<Rigidbody>().detectCollisions = false;

			if (Physics.Raycast(ray, out hit)) {
				
				Damageable ship = hit.collider.GetComponentInParent<Damageable> ();
				if (ship != null) {
					ship.Damage(10, transform);
					
					if (Human) {
						GlobalController.ShowHitMarker.Set();
						GetComponent<AIFighterControl>().Target = ship;
					}
				}
				
			}
		}
	}
}
