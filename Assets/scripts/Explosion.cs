﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {
	public float Size;
	public float Duration;

	// Use this for initialization
	void Start () {
		transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		if (Camera.current != null) {
			transform.rotation = Camera.current.transform.rotation;
		}

		float f = (Size / Duration) * Time.deltaTime;
		transform.localScale += new Vector3 (f, f, f);

		if (transform.localScale.x > Size) {
			Destroy (gameObject);
		}
	}
}
