﻿using UnityEngine;
using System.Collections;
public class GunTurret : Turret {
	private CooldownTimer Shoot;
	public GameObject Tracer;
	public AudioClip ShootSound;
	
	new void Start() {
		DisplayName = "Machine Gun Turret";
		MaxHealth = 200;
		base.Start ();
		
		TargetType = typeof(Personal);
		Shoot = new CooldownTimer (0.1f);
	}
	
	new void Update() {
		base.Update();
		Explosion = GlobalController.ExplSmall;

		
		if (Firing && Shoot.Use) {
			
			RaycastHit hit;
            Vector3 shot = -Barrels.transform.forward;

            if (!Cruiser.PlayerControl.TurretActive(this)) {
                shot = Quaternion.Euler(Random.Range(-3, 3), Random.Range(-3, 3), 0) * shot;
            }

            Ray ray = new Ray(Barrels.position, shot);
			
			if (Physics.Raycast(ray, out hit)) {
				
				Damageable ship = hit.collider.GetComponentInParent<Damageable> ();
				if (ship != null) {
					ship.Damage((Cruiser.PlayerControl.TurretActive(this) ? 10 : 5), transform);
					
					if (Cruiser.PlayerControl.TurretActive(this) || Cruiser.PlayerControl.EnginesActive) {
						GlobalController.ShowHitMarker.Set();
					}
				}
				
			}

			if (Cruiser.PlayerControl.TurretActive(this) || (Cruiser.Human && Cruiser.PlayerControl.DirectTurretMode)) {
				
				AudioSource.PlayClipAtPoint(ShootSound, transform.position);
			}

			GameObject newTracer = Instantiate(Tracer);
			newTracer.transform.position = Barrels.TransformPoint(new Vector3(0.11f, 0.025f, -0.45f)) + ray.direction / 2;
			newTracer.transform.forward = ray.direction;
			newTracer.GetComponent<Rigidbody>().velocity = ray.direction * 100;
			newTracer.GetComponent<Rigidbody>().detectCollisions = false;
			
			newTracer = Instantiate(Tracer);
			newTracer.transform.position = Barrels.TransformPoint(new Vector3(-0.11f, 0.025f, -0.45f)) + ray.direction / 2;
			newTracer.transform.forward = ray.direction;
			newTracer.GetComponent<Rigidbody>().velocity = ray.direction * 100;
			newTracer.GetComponent<Rigidbody>().detectCollisions = false;
		}
	}
	
}
