﻿using UnityEngine;
using System.Collections;

public abstract class Personal : Ship {
    public Camera FirstPersonView { get; private set; }
    public Camera ThirdPersonView { get; private set; }

    //public MeshRenderer FirstPersonModel { get; private set; }
    public MeshRenderer ThirdPersonModel { get; private set; }

    public bool Firing { get; set; }

    protected bool FirstPerson = true;

    new public void Start() {
        base.Start();

        FirstPersonView = transform.FindChild("Camera-1st").GetComponent<Camera>();
        ThirdPersonView = transform.FindChild("Camera-3rd").GetComponent<Camera>();

        //FirstPersonModel = transform.FindChild ("default-1st").GetComponent<MeshRenderer> ();
        ThirdPersonModel = transform.FindChild("default-3rd").GetComponent<MeshRenderer>();

        ReleaseControl();

        if (Team == Team.RED) {
            SpawnFighters.RedFighters++;
        } else {
            SpawnFighters.BlueFighters++;
        }
    }

    new public void Update() {
        base.Update();

        if (!Alive) {
            return;
        }

        if (GlobalController.IsEscort) {
            Cruiser b = Find.FilteredWith<Cruiser>(s => s.name == "BigShip");
            if (b != null) {
                if (transform.position.z < b.transform.position.z - 500 && GlobalController.gameState != GameState.Won) {
                    Destroy();
                }
            }
        }

        if (Input.GetButtonDown("View") && Human) {
            FirstPerson = !FirstPerson;
            UpdateCamera();
        }

        if (Human && !FirstPerson) {
            Vector3 p;
            if (GlobalController.Joystick) {
                p = Util.StickAsMouse("RX", "RY");
            } else {
                p = GlobalController.MouseCoord;
            }
            Vector3 v = new Vector3(-p.x * 4 / Screen.width + 2f, 0, 2);

            if (p.y > Screen.height / 2) {
                v.y = p.y / Screen.height;
            } else {
                v.y = p.y / Screen.height * 3.0f - 1.0f;
            }
            ThirdPersonView.transform.localPosition = v;
        }

        if (Human) {
            if (GlobalController.Joystick) {
                GlobalController.MouseCoord = new Vector2(Screen.width / 2, Screen.height / 2);
            }

            RaycastHit hit;
            if (Physics.Raycast(FirstPersonView.ScreenPointToRay(GlobalController.MouseCoord), out hit)) {
                Damageable s = hit.collider.GetComponentInParent<Damageable>();
                if (s != null) {
                    GlobalController.Targetting = s;
                }
            }
        }

        Health = Mathf.Min(MaxHealth, Health + Time.deltaTime * 2);
    }

    void OnDestroy() {
        if (Team == Team.RED) {
            SpawnFighters.RedFighters--;
        } else {
            SpawnFighters.BlueFighters--;
        }
    }

    void UpdateCamera() {
        Util.SetCameraEnabled(FirstPersonView, FirstPerson && Human);
        //FirstPersonModel.enabled = FirstPerson && Human;
        Util.SetCameraEnabled(ThirdPersonView, !FirstPerson && Human);
        ThirdPersonModel.enabled = !FirstPerson || !Human;
    }

    public override void AssumeControl() {
        Human = true;
        UpdateCamera();
    }

    public override void ReleaseControl() {
        Human = false;
        UpdateCamera();
    }

    new public void OnGUI() {
        base.OnGUI();
        if (Camera.current != null && !Human) {
            Vector3 point = Camera.current.WorldToScreenPoint(transform.position);
            if (point.z > 0) {
                Color c = (Team == Team.BLUE) ? Color.blue : Color.red;
                c.a = 0.8f;
                GUI.color = c;
                GUI.DrawTexture(new Rect(point.x - 16, Screen.height - point.y - 16, 32, 32), GlobalController.FighterOutline);
            }
        }
    }

    void OnCollisionEnter(Collision other) {
        Destroy();
    }
}
