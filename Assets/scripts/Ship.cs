using UnityEngine;
using System.Collections;

public abstract class Ship : Damageable {
	public Team Team = Team.BLUE;
	public bool ShowCrosshair = false;
	public bool Selected = false;
    public bool Ready { get; protected set; }

    public Team OtherTeam {
		get {
			if (Team == Team.RED) {
				return Team.BLUE;
			} else {
				return Team.RED;
			}
		}
	}

	public abstract void AssumeControl();
	public abstract void ReleaseControl();

	new public void Start () {
		base.Start ();
        Ready = true;
		if (Team == Team.RED) {
			foreach (MeshRenderer mesh in GetComponentsInChildren<MeshRenderer>()) {
				foreach (Material material in mesh.materials) {
					if (material.name.ToLower().Contains("highlight")) {
						Color c = material.color;
						c.r = c.b;
						c.b = 0.0f;
						material.color = c;
					}
				}
			}
		}
	}

	public override Team GetTeam() {
		return Team;
	}

	public void OnGUI() {
		if (Human) {
			GUI.color = new Color(1.0f - (Health / MaxHealth), (Health / MaxHealth), 0.0f);
			GUI.DrawTexture(new Rect(50, Screen.height - 82, (Health / MaxHealth) * 128, 32), GlobalController.Healthbar);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(50, Screen.height - 82, 128, 32), GlobalController.HealthbarOutline);

			Shield s = GetComponentInChildren<Shield>();

			if (s != null) {
				GUI.color = new Color(0.0f, 0.5f, 1.0f);
				GUI.DrawTexture(new Rect(50, Screen.height - 132, (s.Health / s.MaxHealth) * 128, 32), GlobalController.Healthbar);
				GUI.color = Color.white;
				GUI.DrawTexture(new Rect(50, Screen.height - 132, 128, 32), GlobalController.HealthbarOutline);
			}

			if (!Alive) {
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), GlobalController.Dead);
			}

			if (!ShowLastHit.Expired) {
				Vector3 p = GlobalController.CurrentCamera.WorldToScreenPoint (LastHit);
				if (p.z > 0) {
					p.y = Screen.height - p.y;
				} else {
					p.x = Screen.width - p.x;
				}
				if (p.x < 0) {
					if (p.y < 0) {
						GUI.DrawTexture(new Rect(0, 0, 256, 256), GlobalController.DamageTL);
					} else if (p.y > Screen.height) {
						GUI.DrawTexture(new Rect(0, Screen.height - 256, 256, 256), GlobalController.DamageBL);
					} else {
						GUI.DrawTexture(new Rect(0, p.y - 128, 256, 256), GlobalController.DamageL);
					}
				} else if (p.x > Screen.width) {
					if (p.y < 0) {
						GUI.DrawTexture(new Rect(Screen.width - 256, 0, 256, 256), GlobalController.DamageTR);
					} else if (p.y > Screen.height) {
						GUI.DrawTexture(new Rect(Screen.width - 256, Screen.height - 256, 256, 256), GlobalController.DamageBR);
					} else {
						GUI.DrawTexture(new Rect(Screen.width - 256, p.y - 128, 256, 256), GlobalController.DamageR);
					}
				} else {
					if (p.y < 0) {
						GUI.DrawTexture(new Rect(p.x - 128, 0, 256, 256), GlobalController.DamageT);
					} else if (p.y > Screen.height) {
						GUI.DrawTexture(new Rect(p.x - 128, Screen.height - 256, 256, 256), GlobalController.DamageB);
					} else {
						if (p.z > 0) {
							GUI.DrawTexture(new Rect(p.x - 128, p.y - 128, 256, 256), GlobalController.Damage);
						} else {
							if (p.x > Screen.width / 2) {
								if (p.y > Screen.height / 2) {
									GUI.DrawTexture(new Rect(Screen.width - 256, Screen.height - 256, 256, 256), GlobalController.DamageBR);
								} else {
									GUI.DrawTexture(new Rect(Screen.width - 256, 0, 256, 256), GlobalController.DamageTR);
								}
							} else {
								if (p.y > Screen.height / 2) {
									GUI.DrawTexture(new Rect(0, Screen.height - 256, 256, 256), GlobalController.DamageBL);
								} else {
									GUI.DrawTexture(new Rect(0, 0, 256, 256), GlobalController.DamageTL);
								}
							}
						}
					}
				}

			}
		}
	}
}

