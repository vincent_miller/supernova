﻿using UnityEngine;
using System.Collections.Generic;

public class GlobalController : MonoBehaviour {
	public static Ship ControlObject { get; private set; }
	public static ExpirationTimer ShowHitMarker;

	public static Texture2D HitMarker { get; private set; }
	public static Texture2D FighterOutline { get; private set; }
	public static Texture2D Healthbar { get; private set; }
	public static Texture2D HealthbarOutline { get; private set; }

	public static Texture2D Damage { get; private set; }

	public static Texture2D DamageL { get; private set; }
	public static Texture2D DamageR { get; private set; }
	public static Texture2D DamageT { get; private set; }
	public static Texture2D DamageB { get; private set; }

	public static Texture2D DamageTL { get; private set; }
	public static Texture2D DamageTR { get; private set; }
	public static Texture2D DamageBL { get; private set; }
	public static Texture2D DamageBR { get; private set; }


	public static Texture2D Dead;

	public static Texture2D CursorTarget;
	public static Texture2D CursorSelect;
	public static Texture2D CursorDefault;

	public static AudioClip SoundBeam;
	public static AudioClip SoundCannon;

	public static GameObject ExplSmall;
    public static GameObject ExplMed;

	public Texture2D InputHitMarker;
	public Texture2D InputFighterOutline;
	public Texture2D InputHealthbar;
	public Texture2D InputHealthbarOutline;

	public Texture2D InputDamage;
	
	public Texture2D InputDamageL;
	public Texture2D InputDamageR;
	public Texture2D InputDamageT;
	public Texture2D InputDamageB;
	
	public Texture2D InputDamageTL;
	public Texture2D InputDamageTR;
	public Texture2D InputDamageBL;
	public Texture2D InputDamageBR;

	public Texture2D InputCursorTarget;
	public Texture2D InputCursorSelect;
	public Texture2D InputCursorDefault;

	public Texture2D InputDead;

	public AudioClip InputSoundBeam;
	public AudioClip InputSoundCannon;

	public GameObject InputExplSmall;
    public GameObject InputExplMed;

    public Texture2D Cycle;
    public Texture2D CycleDeselected;
    public Texture2D[] CycleIcons;
    private int currentCycleSelection;

    public GUISkin skin;
    public Material[] dummies;

    public static bool ShowCursor = true;

	public static Camera CurrentCamera { get; set; }

	public Texture2D Crosshair;
	float deltaTime = 0.0f;

	public static Texture2D CurrentCursor;

	public static bool Paused = false;
	private static Menu CurrentMenu = PauseMenu;
	private delegate void Menu ();

	private static AudioSource Music;

	public static Damageable Targetting;

	public static bool Joystick { get; private set; }

    public bool escort = false;
    public static bool IsEscort;

    public static GameState gameState = GameState.Playing;

	public static Vector2 MouseCoord {
		get {
            return Input.mousePosition;
			//return curMouse;
		}

		set {
			curMouse.x = Mathf.Clamp(value.x, 0, Screen.width);
			curMouse.y = Mathf.Clamp(value.y, 0, Screen.height);
		}
	}

	private static Vector2 lastMouse;
	private static Vector2 curMouse;


	public static Camera SpawnCamera {
		get {
			return GameObject.Find ("SpawnCamera").GetComponent<Camera> ();
		}
	}

	private static void PauseMenu () {
		if (GUI.Button(new Rect(Screen.width / 2 - 100, 100, 200, 50), "Resume Game")) {
			Paused = false;
			Time.timeScale = 1.0f;
		}
		
		if (GUI.Button(new Rect(Screen.width / 2 - 100, 175, 200, 50), "Options")) {
			CurrentMenu = OptionsMenu;
		}

		if (GUI.Button(new Rect(Screen.width / 2 - 100, 250, 200, 50), "Restart Map")) {
			Application.LoadLevel(Application.loadedLevelName);
		}
		
		if (GUI.Button(new Rect(Screen.width / 2 - 100, 325, 200, 50), "Exit Game")) {
			Application.Quit ();
		}

		if (GUI.Button(new Rect(Screen.width / 2 - 100, 400, 200, 50), "Main Menu")) {
			Application.LoadLevel ("MainMenu");
		}
	}

	private static void OptionsMenu () {
		if (GUI.Button(new Rect(Screen.width / 2 - 100, 100, 200, 50), "Back")) {
			CurrentMenu = PauseMenu;
		}

		Vector2 s1 = GUI.skin.label.CalcSize (new GUIContent ("Fighter Sensitivity"));

		GUI.Label (new Rect (Screen.width / 2 - s1.x - 125, 262 - s1.y / 2, s1.x, s1.y), "Fighter Sensitivity");
		PlayerPrefs.SetFloat("Sensitivity", GUI.HorizontalSlider (new Rect (Screen.width / 2 - 100, 250, 200, 50), PlayerPrefs.GetFloat("Sensitivity", 1.0f), 0.0f, 1.0f));

		Vector2 s2 = GUI.skin.label.CalcSize (new GUIContent ("Show FPS"));
		GUI.Label (new Rect (Screen.width / 2 - s2.x - 125, 350 - s2.y / 2, s2.x, s2.y), "Show FPS");
		PlayerPrefs.SetInt("ShowFPS", GUI.Toggle (new Rect (Screen.width / 2 - 25, 325, 50, 50), PlayerPrefs.GetInt("ShowFPS", 0) == 1, "") ? 1 : 0);

		Vector2 s3 = GUI.skin.label.CalcSize (new GUIContent ("Music Volume"));
		GUI.Label (new Rect (Screen.width / 2 - s3.x - 125, 462 - s3.y / 2, s3.x, s3.y), "Music Volume");
		PlayerPrefs.SetFloat("MusicVolume", GUI.HorizontalSlider (new Rect (Screen.width / 2 - 100, 450, 200, 50), PlayerPrefs.GetFloat("MusicVolume", 0.5f), 0.0f, 1.0f));

		/*Vector2 s4 = GUI.skin.label.CalcSize (new GUIContent ("Toon Shading"));
		GUI.Label (new Rect (Screen.width / 2 - s4.x - 125, 550 - s4.y / 2, s4.x, s4.y), "Toon Shading");
		PlayerPrefs.SetInt("Toon", GUI.Toggle (new Rect (Screen.width / 2 - 25, 525, 50, 50), PlayerPrefs.GetInt("Toon", 0) == 1, "") ? 1 : 0);*/
		
		Music.volume = PlayerPrefs.GetFloat ("MusicVolume", 0.5f);
	}

	public static void ReleaseControl() {
		if (ControlObject != null) {
			ControlObject.ReleaseControl ();
			ControlObject = null;
		}

		Util.SetCameraEnabled(CurrentCamera, false);

		Targetting = null;

		Util.SetCameraEnabled(SpawnCamera, true);
		CurrentCamera = SpawnCamera;
		
		Cursor.lockState = CursorLockMode.None;
		ShowCursor = false;
	}

	public static void AssumeControl(Ship ship) {
		if (Paused) {
			return;
		}

		ReleaseControl ();

		if (ship != null && ship.Ready) {
			ship.AssumeControl ();
			ControlObject = ship;
			Util.SetCameraEnabled(SpawnCamera, false);
		}
	}

	public static void AssumeControl<T> () where T : Ship {
		AssumeControl (Find.BestFilteredWith<T> (s => s.Team == Team.BLUE && s.Ready, s => s.Health));
	}

	void Start () {
		Joystick = Input.GetJoystickNames().Length > 0;
		MouseCoord = new Vector2(Screen.width / 2, Screen.height / 2);
		lastMouse = Input.mousePosition;
		Music = GetComponent<AudioSource> ();
		
		Music.volume = PlayerPrefs.GetFloat ("MusicVolume", 0.5f);
		ShowHitMarker = new ExpirationTimer(0.08f);
		HitMarker = InputHitMarker;
		FighterOutline = InputFighterOutline;
		Healthbar = InputHealthbar;
		HealthbarOutline = InputHealthbarOutline;

		Damage = InputDamage;
		
		DamageR = InputDamageR;
		DamageL = InputDamageL;
		DamageT = InputDamageT;
		DamageB = InputDamageB;
		
		DamageTL = InputDamageTL;
		DamageTR = InputDamageTR;
		DamageBL = InputDamageBL;
		DamageBR = InputDamageBR;

		CursorDefault = InputCursorDefault;
		CursorSelect = InputCursorSelect;
		CursorTarget = InputCursorTarget;

		CurrentCursor = InputCursorDefault;
		CurrentCamera = SpawnCamera;

		Dead = InputDead;

		SoundBeam = InputSoundBeam;
		SoundCannon = InputSoundCannon;

		ExplSmall = InputExplSmall;
        ExplMed = InputExplMed;

        IsEscort = escort;
		//Time.timeScale = 0.1f;
	}

	void OnGUI () {
		GUI.skin = skin;

        if (!Paused) {
            GUI.skin.label.fontSize = 48;
            if (gameState == GameState.Won) {
                Vector2 s1 = GUI.skin.label.CalcSize(new GUIContent("Mission Success"));
                GUI.color = Color.blue;
                GUI.Label(new Rect(Screen.width / 2 - s1.x / 2, Screen.height / 2 - s1.y / 2, s1.x, s1.y), "Mission Success");
                return;
            } else if (gameState == GameState.Lost) {
                Vector2 s1 = GUI.skin.label.CalcSize(new GUIContent("Mission Failure"));
                GUI.color = Color.red;
                GUI.Label(new Rect(Screen.width / 2 - s1.x / 2, Screen.height / 2 - s1.y / 2, s1.x, s1.y), "Mission Failure");
                return;
            }
            GUI.skin.label.fontSize = 24;
        }

        GUI.color = Color.white;


        if (!ShowHitMarker.Expired) {
			if (!ShowCursor) {
				GUI.DrawTexture(new Rect(Screen.width / 2 - 16, Screen.height / 2 - 16, 32, 32), HitMarker);
			} else {
				Vector3 v = MouseCoord;
				v.y = Screen.height - v.y;
				GUI.DrawTexture(new Rect(v - new Vector3(16, 16), new Vector3(32, 32)), HitMarker);
			}
		}

		if (Targetting != null) {
			if (Targetting.GetTeam() == Team.BLUE) {
				GUI.color = Color.blue;
			} else {
				GUI.color = Color.red;
			}

			GUI.Label(new Rect(Screen.width / 2 - 64, Screen.height / 2 + 150, 512, 64), Targetting.DisplayName);

			GUI.DrawTexture(new Rect(Screen.width / 2 - 64, Screen.height / 2 + 128, (Targetting.Health / Targetting.MaxHealth) * 128, 16), Healthbar);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(Screen.width / 2 - 64, Screen.height / 2 + 128, 128, 16), HealthbarOutline);
		}

		int w = Screen.width, h = Screen.height;
		
		GUIStyle style = new GUIStyle();
		
		Rect rect = new Rect(0, 0, w, h * 2 / 100);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h / 40;
		style.normal.textColor = new Color (1.0f, 0.0f, 0.0f, 1.0f);
		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;

		if (PlayerPrefs.GetInt("ShowFPS", 0) == 1) {
			string text = string.Format ("{0:0.0} ms ({1:0.} fps)", msec, fps);
			GUI.Label (rect, text, style);
		}

		if (ControlObject != null && ControlObject.ShowCrosshair && !Paused) {
			GUI.DrawTexture (new Rect (Screen.width / 2 - 16, Screen.height / 2 - 16, 32, 32), Crosshair);
		}

		if (Paused) {
			CurrentCursor = CursorDefault;
			ShowCursor = false;
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			CurrentMenu ();
		}

		if (ShowCursor) {
			Vector3 v = GlobalController.MouseCoord;
			v.y = Screen.height - v.y;
			GUI.DrawTexture(new Rect(v - new Vector3(32, 32), new Vector3(64, 64)), CurrentCursor);
		}

        if (Input.GetKey(KeyCode.Tab) && !Paused) {
            int types = 6;
            Vector2 sizeIcon = new Vector2(128, 128);
            Vector2 sizeOutline = new Vector2(200, 200);
            Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);

            for (int i = 0; i < types; i++) {
                float angle = i / ((float) types) * 2 * Mathf.PI;
                Vector2 pos = new Vector2(Mathf.Sin(angle) * 200, Mathf.Cos(angle) * 200);

                GUI.DrawTexture(new Rect(center + pos - sizeOutline / 2, sizeOutline), currentCycleSelection == i ? Cycle : CycleDeselected);
                GUI.DrawTexture(new Rect(center + pos - sizeIcon / 2, sizeIcon), CycleIcons[i]);
            }

        }

	}
	
	// Update is called once per frame
	void Update () {

		Vector2 mouse = Input.mousePosition;

		MouseCoord += new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")) * PlayerPrefs.GetFloat("Sensitivity") * 20;
		lastMouse = mouse;

        if (Input.GetKey(KeyCode.Tab)) {
            Vector2 inp = Input.mousePosition - new Vector3(Screen.width, Screen.height) / 2;
            if (inp.sqrMagnitude > 0) {
                float circle = 2 * Mathf.PI;
                float angle = Mathf.Atan2(inp.x, -inp.y);
                currentCycleSelection = ((Mathf.RoundToInt(angle * 6 / circle) % 6) + 6) % 6;
            }
        }

        if (Input.GetKeyUp(KeyCode.Tab)) {
            switch (currentCycleSelection) {
                case 0:
                    AssumeControl<Bomber>();
                    break;
                case 1:
                    AssumeControl<Fighter>();
                    break;
                case 2:
                    AssumeControl<Frigate>();
                    break;
                case 3:
                    AssumeControl<Destroyer>();
                    break;
                case 4:
                    AssumeControl<Carrier>();
                    break;
                case 5:
                    ReleaseControl();
                    break;
            }
        }


		//print (Input.GetButton("Fire"));
		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (Paused) {
				Time.timeScale = 1;
				Paused = false;
			} else {
				Time.timeScale = 0;
				Paused = true;
			}
		}

		if (Paused) {
			return;
		}
		Cursor.visible = false;
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		if (Input.GetKeyDown (KeyCode.F1)) {
			AssumeControl<Fighter>();
		}

		if (Input.GetKeyDown (KeyCode.F2)) {
			AssumeControl<Bomber>();
		}

		if (Input.GetKeyDown (KeyCode.F3)) {
			AssumeControl<Frigate>();
		}

		if (Input.GetKeyDown (KeyCode.F4)) {
			AssumeControl<Destroyer>();
		}

		if (Input.GetKeyDown (KeyCode.F5)) {
			AssumeControl<Carrier>();
		}

		if (Input.GetKeyDown (KeyCode.F6)) {
			ReleaseControl ();
		}

		if (CurrentCamera != SpawnCamera && SpawnCamera.GetComponent<AudioListener> ().enabled) {
			SpawnCamera.GetComponent<AudioListener>().enabled = false;
		}

        bool bigShipAlive = Find.FilteredWith<Cruiser>(c => c.name == "BigShip");

        if (escort) {
            foreach (Cruiser c in Find.AllWith<Cruiser>()) {
                if (c.name != "BigShip" && !c.PlayerControl.EnginesActive && !c.AIControl.HasDirectives() && bigShipAlive) {
                    c.AIControl.AddDirective(new AIFollowEscort());
                }

            }
        }

	}
}
