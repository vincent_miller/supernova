﻿using UnityEngine;
using System.Collections.Generic;

public class Cruiser : Ship {
	private Engine[] Engines;
	private Turret[] Turrets;
	public PlayerCruiserControl PlayerControl { get; private set; }
	public AICruiserControl AIControl { get; private set; }

    public bool JumpIn = false;
    private Vector3 initialPos;
    public bool Jumping = false;

    private float cameraExtent;

	public float COMY;
	public Camera View { get; private set; }

    private CooldownTimer Retaliate;

	public int NumTurrets {
		get {
			return Turrets.Length;
		}
	}

	public void UpdateTurretList() {
		List<Turret> turretsTemp = new List<Turret> ();
		List<Engine> enginesTemp = new List<Engine> ();

		for (int i = 0; i < transform.childCount; i++) {
			GameObject obj = transform.GetChild(i).gameObject;
			Engine engine = obj.GetComponent<Engine>();
			Turret turret = obj.GetComponent<Turret>();
			
			if (engine != null && engine.Alive) {
				enginesTemp.Add (engine);
			}
			
			if (turret != null && turret.Alive) {
				turretsTemp.Add (turret);
			}
		}

		
		Turrets = turretsTemp.ToArray();
		Engines = enginesTemp.ToArray ();
	}
	
	public override void AssumeControl() {
		Human = true;
		PlayerControl.UpdateCamera ();
	}
	
	public override void ReleaseControl() {
		Human = false;
		PlayerControl.UpdateCamera ();
	}
	
	
	void OnMouseDown() {
		if (GlobalController.ControlObject != null && !GlobalController.ControlObject.ShowCrosshair) {
            if (GlobalController.ControlObject is Cruiser && ((Cruiser)GlobalController.ControlObject).PlayerControl.DirectTurretMode) {
                return;
            } else if (!Human && Team == Team.BLUE) {
				GlobalController.AssumeControl (this);
			} else if (Team == Team.RED && GlobalController.ControlObject is Cruiser) {
				Cruiser other = ((Cruiser)GlobalController.ControlObject);
				other.AIControl.ClearDirectives ();
				other.AIControl.AddDirective (new AIAttackTarget (this));
			}
		}
	}

	// Use this for initialization
	new public void Start () {
		base.Start ();
        Retaliate = new CooldownTimer(20.0f);
        gameObject.AddComponent<PlayerCruiserControl>();

        if (name != "BigShip") {
            gameObject.AddComponent<AICruiserControl>();
        }

		Rigidbody rb = GetComponent<Rigidbody> ();
        if (rb != null) {
            Vector3 com = rb.centerOfMass;
            com.y = COMY;
            rb.centerOfMass = com;
            rb.maxAngularVelocity = 4;
        }


		UpdateTurretList ();


		View = transform.FindChild ("Camera").GetComponent<Camera> ();
        cameraExtent = View.transform.localPosition.magnitude;
        PlayerControl = GetComponent<PlayerCruiserControl> ();
		AIControl = GetComponent<AICruiserControl> ();

		ReleaseControl ();

		Transform t = transform.FindChild ("Shield");
		if (t != null) {
			t = t.FindChild ("Outside");
			Material m = t.GetComponent<MeshRenderer> ().material;
			m.renderQueue = m.shader.renderQueue + 1;
		}

        Ready = !JumpIn;

        if (JumpIn) {
            initialPos = transform.position;
            transform.position += transform.forward * 10000;
        }
	}

    public void ActivateJump() {
        Jumping = true;
    }

	public void UpdateThrust(Vector3 desiredThrust, float thrust) {
		foreach (Engine engine in Engines) {
			engine.UpdateThrust (desiredThrust, thrust);
		}
	}

	public void UpdateTorque(Vector3 desiredTorque, float thrust) {
		foreach (Engine engine in Engines) {
			engine.UpdateTorque (desiredTorque, thrust);
		}
	}

	public void CancelThrust() {
		foreach (Engine engine in Engines) {
			engine.CancelThrust();
		}
	}

	public int TurretIndex (Turret turret) {
		for (int i = 0; i < NumTurrets; i++) {
			if (Turrets[i] == turret) {
				return i;
			}
		}

		return -1;
	}

	public Turret TurretAt(int index) {
		return Turrets [index];
	}

	public bool ThrustReady() {
		foreach (Engine engine in Engines) {
			if (!engine.Ready) {
				return false;
			}
		}

		return true;
	}

	new void OnGUI() {
		base.OnGUI ();
		if (GlobalController.ControlObject == null && Selected) {
			Vector3 point = GlobalController.SpawnCamera.WorldToScreenPoint (transform.position);
			if (point.z > 0) {
				Color c = Color.yellow;
				c.a = 0.8f;
				GUI.color = c;
				GUI.DrawTexture (new Rect (point.x - 32, Screen.height - point.y - 32, 64, 64), GlobalController.FighterOutline);
				GUI.color = Color.blue;
				GUI.DrawTexture(new Rect(point.x - 64, Screen.height - point.y + 64, (Health / MaxHealth) * 128, 16), GlobalController.Healthbar);
				GUI.color = Color.white;
				GUI.DrawTexture(new Rect(point.x - 64, Screen.height - point.y + 64, 128, 16), GlobalController.HealthbarOutline);
			}
		}
	}

    protected override void OnTakeDamage(float damage, Transform source) {
        Ship s = source.GetComponent<Ship>();
        if (Retaliate.Use) {
            foreach (Turret t in Turrets) {
                t.TryTarget(s);
            }
        }
    }

	new public void Update() {
		if (GlobalController.Paused) {
			return;
		}

		base.Update();

        if (Jumping) {
            transform.position = Vector3.Lerp(transform.position, initialPos, Time.deltaTime * 4);
            if (Vector3.Distance(transform.position, initialPos) < 2) {
                Jumping = false;
                Ready = true;
            }
        }

		if (Human && PlayerControl.EnginesActive && Alive) {
			if (Input.GetMouseButton(1)) {
				Cursor.lockState = CursorLockMode.Locked;
				View.transform.RotateAround (transform.position, transform.up, Input.GetAxis("Mouse X"));
				View.transform.RotateAround (transform.position, View.transform.right, -Input.GetAxis("Mouse Y"));
				GlobalController.ShowCursor = false;

			} else {
				Cursor.lockState = CursorLockMode.None;
				GlobalController.ShowCursor = true;
			}

			float f = Input.GetAxis("Mouse ScrollWheel");

			if (f != 0) {
				float l = View.transform.localPosition.magnitude;
				l -= f * 10;
                l = Mathf.Clamp(l, cameraExtent * 0.6f, cameraExtent * 3.0f);
				View.transform.localPosition = View.transform.localPosition.normalized * l;
			}

            if (PlayerControl.DirectTurretMode) {
                GlobalController.CurrentCursor = GlobalController.CursorTarget;
            } else {
                GlobalController.CurrentCursor = GlobalController.CursorDefault;
            }
            RaycastHit hit;

            if (Physics.Raycast(View.ScreenPointToRay(GlobalController.MouseCoord), out hit)) {
                GameObject target = hit.transform.gameObject;
                Cruiser cruiser = target.GetComponent<Cruiser>();
                if (cruiser != null) {
                    GlobalController.Targetting = cruiser;
                    if (!PlayerControl.DirectTurretMode) {
                        if (cruiser.Team == Team.BLUE) {
                            GlobalController.CurrentCursor = GlobalController.CursorSelect;
                        } else {
                            GlobalController.CurrentCursor = GlobalController.CursorTarget;
                        }
                    }
                }
                
            }
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Rigidbody rb = GetComponent<Rigidbody> ();
		if (rb != null && rb.velocity.sqrMagnitude > 100) {
			rb.velocity = rb.velocity.normalized * 10;
		}
	}
}
