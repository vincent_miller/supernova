﻿using UnityEngine;
using System.Collections;
using System;

public class BeamTurret : Turret {
	private LineRenderer LeftBeam;
	private LineRenderer RightBeam;

	new void Start() {
		MaxHealth = 200;
		DisplayName = "Particle Beam Turret";
		base.Start ();
		LeftBeam = Barrels.FindChild("LeftBeam").GetComponent<LineRenderer> ();
		RightBeam = Barrels.FindChild("RightBeam").GetComponent<LineRenderer> ();

		AudioSource src = gameObject.AddComponent<AudioSource> ();
		src.loop = true;

		TargetType = typeof(Personal);
	}

	new void Update() {
		base.Update();
		Explosion = GlobalController.ExplSmall;

		AudioSource src = GetComponent<AudioSource> ();
		if (src.clip == null) {
			src.clip = GlobalController.SoundBeam;
		}

		LeftBeam.enabled = Firing;
		RightBeam.enabled = Firing;

		if (Firing) {

			RaycastHit hit;

			if (Physics.Raycast(new Ray(Barrels.position, -Barrels.forward), out hit)) {
				LeftBeam.SetPosition(1, new Vector3(0, 0, -hit.distance));
				RightBeam.SetPosition(1, new Vector3(0, 0, -hit.distance));

				Damageable ship = hit.collider.GetComponentInParent<Damageable> ();
				if (ship != null) {
					ship.Damage((Cruiser.PlayerControl.TurretActive(this) ? 40 : 10) * Time.deltaTime, transform);

					if (Cruiser.PlayerControl.TurretActive(this) || Cruiser.PlayerControl.EnginesActive) {
						GlobalController.ShowHitMarker.Set();
					}
				}

			} else {
				LeftBeam.SetPosition(1, new Vector3(0, 0, -100));
				RightBeam.SetPosition(1, new Vector3(0, 0, -100));
			}
		}

		if (Input.GetMouseButtonUp (0) && GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().Stop();
		}

		if (Input.GetMouseButtonDown (0) && (Cruiser.PlayerControl.TurretActive (this) || (Cruiser.Human && Cruiser.PlayerControl.DirectTurretMode))) {
			GetComponent<AudioSource>().Play();
		}
	}

}
