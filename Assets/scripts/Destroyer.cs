﻿using UnityEngine;
using System.Collections;

public class Destroyer : Cruiser {

    new public void Update() {
        base.Update();
        //if (GlobalController.IsEscort && Team == Team.RED && !AIControl.HasDirectives()) {
         ///   Cruiser c = PickTarget();
        //    if (c != null) {
        //        //AIControl.AddDirective(new AIAttackTarget(c));
        //    }
      //  }
    }

    private Cruiser PickTarget() {
        return Find.NearestFilteredWith<Cruiser>(f => f.Team == OtherTeam && Vector3.SqrMagnitude(transform.position - f.transform.position) < 200 * 200, transform.position);
    }
}
