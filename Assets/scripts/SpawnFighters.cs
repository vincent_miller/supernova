﻿using UnityEngine;
using System.Collections;

public class SpawnFighters : MonoBehaviour {
	public GameObject Fighter;
	public GameObject Bomber;
	private CooldownTimer SpawnTimer;
	private Vector3 Position = new Vector3(4, 0, 0);
	private int Type;

	public static int BlueFighters;
	public static int RedFighters;

	// Use this for initialization
	void Start () {
		SpawnTimer = new CooldownTimer (2.0f);
	}
	
	// Update is called once per frame
	void Update () {
		Cruiser s = GetComponent<Cruiser> ();
		if (s.Ready && SpawnTimer.Use) {
			if (((s.Team == Team.BLUE && BlueFighters < 15) || (s.Team == Team.RED && RedFighters < 15))) {
				GameObject obj;
				if (Type % 3 > 0) {
					obj = Instantiate (Fighter);
				} else {
					obj = Instantiate (Bomber);
				}
				Type++;
				obj.transform.position = transform.TransformPoint (Position);
				obj.GetComponent<Rigidbody> ().velocity = (obj.transform.position - transform.position).normalized * 10 + GetComponent<Rigidbody>().velocity;
				obj.transform.forward = -obj.GetComponent<Rigidbody> ().velocity;
				Position.x *= -1;
			}
		}
	}
}
