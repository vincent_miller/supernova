﻿using UnityEngine;
using System.Collections;

public class TriggerJump : MonoBehaviour {
    public Cruiser[] Targets;
    public float delay;
    public bool activate;

    void OnTriggerEnter(Collider other) {
        Cruiser c = other.GetComponentInParent<Cruiser>();
        if (c != null && c.name == "BigShip"){
            activate = true;
        }
    }
	
	void Update () {
	    if (activate) {
            Invoke("Jump", delay);
        }
	}

    void Jump() {
        foreach (Cruiser c in Targets) {
            c.ActivateJump();
            activate = false;
        }
        Destroy(gameObject);
    }
}
