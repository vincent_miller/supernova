##################################################################################
Credits:
##################################################################################

==================================================================================
Skyboxes:
==================================================================================

Skybox Volume 2 (Nebula), by Hedgehog Team
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/3392

==================================================================================
Sound Effects:
==================================================================================

Dark Future Music, by Affordable Audio 4 Everyone
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/3777

Futuristic Weapons Set, by Raffaele
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/15644

Laser Beam
http://www.sounddogs.com/previews/2226/mp3/511846_SOUNDDOGS__sc.mp3

Artillery Shot
http://soundbible.com/7-Anti-Aircraft-Gun.html
license: https://creativecommons.org/licenses/by/3.0/us/

==================================================================================
Effects:
==================================================================================

Detonator Explosion Framework, by Ben Throop
https://www.assetstore.unity3d.com/en/#!/content/1

==================================================================================
Textures:
==================================================================================

RockSharp0041, by Textures.com
http://www.textures.com/download/rocksharp0041/67484

==================================================================================
Font:
==================================================================================

Airstrike by Iconian Fonts
http://www.dafont.com/airstrike.font

==================================================================================
All other assets created by me (unless I forgot something)
==================================================================================

##################################################################################
Controls:
##################################################################################

==================================================================================
Global:
==================================================================================

Ship Selection Wheel -- Tab
Control Fighter ------- F1
Control Bomber -------- F2
Control Frigate ------- F3
Control Destroyer ----- F4
Control Carrier ------- F5

==================================================================================
Fighter / Bomber:
==================================================================================

Thrust Forward -------- W
Thrust Backward ------- S
Roll Left ------------- A
Roll Right ------------ D
Pitch/Yaw ------------- Move Mouse
Shoot ----------------- LMB
Toggle View ----------- V
Bomber Reload --------- R
Afterburner ----------- LShift

==================================================================================
Cruiser:
==================================================================================

Pitch Down ------------ W
Pitch Up -------------- S
Roll Left ------------- A
Roll Right ------------ D
Yaw Left -------------- Q
Yaw Right ------------- E
Thrust Forward -------- LShift
Thrust Backward ------- LCtrl
Thrust Up ------------- R
Thrust Down ----------- F
Previous Seat --------- 1
Next Seat ------------- 2
Toggle Auto Turrets --- X
Fire All Turrets ------ LMB in Non-Auto Mode
Rotate Camera --------- RMB + Drag
Zoom Camera ----------- Mouse Wheel
Attack Target --------- LMB on Enemy in Auto Mode
Control Target -------- LMB on Friendly in Auto Mode

==================================================================================
Turret:
==================================================================================

Aim ------------------- Move Mouse
Fire ------------------ LMB
Previous Seat --------- 1
Next Seat ------------- 2

##################################################################################
Feedback, Comments, Suggestions:
##################################################################################

Contact me!

Vincent Miller
vmiller0725@gmail.com