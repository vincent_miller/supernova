﻿Shader "Custom/ParticleBeam" {
	Properties {
		_OutColor ("Outer Color", Color) = (1,0,0,1)
		_InColor ("Inner Color", Color) = (1,0.5,0,1)
	}
	
	SubShader {
		
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
		
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			fixed4 _OutColor;
			fixed4 _InColor;
			
			struct v2f {
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata_base v) {
                v2f o;
                o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                float y = i.uv.y;
                float d = abs(y - 0.5) * 2;
                d += abs(sin(_Time * 360) * 0.2);
                fixed4 c = d * _OutColor + (1.0 - d) * _InColor;
                c *= 5;
                c.a = 1.0f - d;
                //c.r /= d;
                //c.g /= d;
                //c.b /= d;
                
                return c;
                
            }
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
