﻿Shader "Custom/Blast" {
	Properties {
		_Color ("Color", Color) = (1,0.5,0,0)
	}
	
	SubShader {
		
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
		
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			
			struct v2f {
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata_base v) {
                v2f o;
                o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                float y = i.uv.y;
                float dy = abs(y - 0.5) * 2;
               	float x = i.uv.x;
               	
                fixed4 c = _Color;
               	
           		float dx = abs(x - 0.9) * 10;
               	if (x > 0.9) {
               		c.a = 1.0f - pow(length(float2(dx, dy)), 2);
           		} else {
               	 	c.a = (1.0f - pow(dy, 2)) * (1.0f - pow(dx / 9, 2));
           		}
               	
                //c.r /= d;
                //c.g /= d;
                //c.b /= d;
                
                return c;
                
            }
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
