﻿Shader "Custom/Explosion" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
	}
	SubShader {
		Tags {
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Pass {
		
			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			fixed4 _Color;
			
			struct v2f {
				float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
			};
			
			v2f vert (appdata_base v) {
                v2f o;
                o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
                o.uv = v.texcoord;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                float d = distance(i.uv, float2(0.5, 0.5));
                float f = pow(0.3 / (d - 0.7), 2);
                if (d < 0.5) {
                	return _Color * f;
            	} else {
            		return fixed4(0, 0, 0, 0);
        		}
            }
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
