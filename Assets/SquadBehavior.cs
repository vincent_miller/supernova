﻿using UnityEngine;
using System.Collections;

public class SquadBehavior : MonoBehaviour {

	public SquadBehavior leader;
	private Rigidbody rb;

	private Vector3 leaderDest;

	private Vector3 dest {
		get {
			return leader == null ? leaderDest : leader.dest;
		}
	}

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		PickDest ();
	}

	void PickDest() {
		leaderDest = new Vector3 (Random.Range (-50, 50), Random.Range (-50, 50), Random.Range (-50, 50));
	}

	void OnCollisionEnter(Collision col) {
		print ("Crash");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 vmod = Vector3.zero;

		transform.forward = Vector3.RotateTowards(transform.forward, dest - transform.position, 2.0f * Time.deltaTime, 0.0f);
		
		if (leader == null && Vector3.Distance(transform.position, dest) < 5) {
			PickDest();
		}

		if (leader != null) {
			Vector3 v = leader.transform.position - transform.position;
			vmod += (v.magnitude - 2) * v.normalized;

			foreach (SquadBehavior s in Find.AllWith<SquadBehavior>()) {
				if (s != this && s.leader == leader) {
					v = s.transform.position - transform.position;
					vmod += (v.magnitude - 4) * v.normalized;
				}
			}

			float posOnLeaderZ = Vector3.Dot (-leader.transform.forward, transform.position - leader.transform.position);
			vmod += (posOnLeaderZ - 2) * leader.transform.forward;

			if (leader.leader != null) {
				Vector3 loll = Vector3.ProjectOnPlane (leader.transform.position - leader.leader.transform.position, leader.leader.transform.forward);
				Vector3 mol = Vector3.ProjectOnPlane (transform.position - leader.transform.position, leader.leader.transform.forward);
				vmod += loll - mol;
			}
		}



		rb.velocity = transform.forward * 20 + vmod * 5;
	}
}
