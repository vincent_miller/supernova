#Credits:

##Skyboxes:

Menu:
Skybox Volume 2 (Nebula), by Hedgehog Team
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/3392

Game skybox created with SpaceScape
http://alexcpeterson.com/spacescape/

##Sound Effects:

Dark Future Music, by Affordable Audio 4 Everyone
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/3777

Futuristic Weapons Set, by Raffaele
https://www.assetstore.unity3d.com/en/?gclid=Cj0KEQjwyK-vBRCp4cymxermx-EBEiQATOQgh19D8aacp2pa5tPwqTcBuqapUjS-yhLrdgnqJvXk_6caAr8q8P8HAQ#!/content/15644

Laser Beam
https://www.freesound.org/people/bubaproducer/sounds/151022/

Artillery Shot
http://soundbible.com/7-Anti-Aircraft-Gun.html
license: https://creativecommons.org/licenses/by/3.0/us/

Lock-on (and other cockpit beeps)
http://www.freesound.org/people/Alxy/sounds/189327/

Rocket Thrust
https://www.freesound.org/people/LimitSnap_Creations/sounds/318688/

Flare Deploy
https://www.freesound.org/people/Goup_1/sounds/171148/

Impact
https://www.freesound.org/people/mickyman5000/sounds/340493/

Fighter Weapon
https://www.freesound.org/people/kantouth/sounds/104401/

##Visual Effects:

Detonator Explosion Framework, by Ben Throop
https://www.assetstore.unity3d.com/en/#!/content/1

##Textures:

RockSmooth0015, by Textures.com
http://www.textures.com/download/rocksmooth0015/5648

MetalAircraft0061, Textures.com
http://www.textures.com/download/metalaircraft0061/50035

Grungemaps0136
http://www.textures.com/download/grungemaps0136/45589

Planet
http://pics-about-space.com/planets-hd-texture?p=3#img10294528661026407140

##Font:

Conthrax by Typodermic Fonts
http://www.dafont.com/conthrax.font


##Base Code for Atmosphere Shader:

http://forum.unity3d.com/threads/planet-atmosphere-shader-help.180750/

#Controls:

##Global:

Action | Key
-------|----
Ship Selection Wheel | Tab
Control Fighter | F1
Control Bomber | F2
Control Frigate | F3
Control Destroyer | F4
Control Carrier | F5

##Fighter / Bomber:

Action | Key
-------|----
Thrust Forward | W
Thrust Backward | S
Roll Left | A
Roll Right | D
Countermeasures | E
Lock Target | Q
Pitch/Yaw | Move Mouse
Shoot | LMB
Missile | RMB
Toggle View | V
Bomber Reload | R
Afterburner | LShift

##Cruiser:

Action | Key
-------|----
Forward | W
Reverse | S
Left | A
Right | D
Yaw Left | Q
Yaw Right | E
Toggle Auto Turrets | X
Fire All Turrets | LMB in Non-Auto Mode
Rotate Camera | Move Mouse

#Feedback, Comments, Suggestions:

Contact me!

Vincent Miller
vmiller0725@gmail.com